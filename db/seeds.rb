# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#Question.delete_all
if Question.count == 0
	Question.create(
		JSON.load(open("#{Rails.root}/db/seeds/questions.json"))
	)
end

if City.count == 0
	City.create(
			JSON.load(open("#{Rails.root}/db/seeds/cities.json"))
	)
end