# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170915125026) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "city_links", force: :cascade do |t|
    t.string   "category"
    t.text     "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "city_id"
  end

  add_index "city_links", ["city_id"], name: "index_city_links_on_city_id", using: :btree

  create_table "committal_evolutions", force: :cascade do |t|
    t.integer  "committal_id"
    t.decimal  "orcamento"
    t.decimal  "empenhado"
    t.decimal  "liquidado"
    t.decimal  "executado_financeiro"
    t.decimal  "percentual_contratado"
    t.decimal  "executado_vs_contratado"
    t.decimal  "saldo"
    t.decimal  "executado_fisico"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "committal_evolutions", ["committal_id"], name: "index_committal_evolutions_on_committal_id", using: :btree

  create_table "committals", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "indicator_id"
    t.string   "photo"
    t.date     "start_date"
    t.date     "estimate_date"
    t.decimal  "orcamento"
    t.decimal  "empenhado"
    t.decimal  "liquidado"
    t.decimal  "executado_financeiro"
    t.decimal  "percentual_contratado"
    t.decimal  "executado_vs_contratado"
    t.decimal  "saldo"
    t.decimal  "executado_fisico"
    t.integer  "probabilidade"
    t.decimal  "changes_amount"
  end

  add_index "committals", ["indicator_id"], name: "index_committals_on_indicator_id", using: :btree

  create_table "decisions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "rae_id"
    t.text     "description"
    t.string   "status"
    t.date     "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "decisions", ["rae_id"], name: "index_decisions_on_rae_id", using: :btree
  add_index "decisions", ["user_id"], name: "index_decisions_on_user_id", using: :btree

  create_table "goals", force: :cascade do |t|
    t.integer  "indicator_id"
    t.date     "date"
    t.decimal  "value"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "goals", ["indicator_id"], name: "index_goals_on_indicator_id", using: :btree

  create_table "group_statistics", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "icon"
    t.string   "color"
    t.boolean  "remove"
  end

  create_table "histories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "user_id"
    t.boolean  "finish"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "status"
    t.integer  "step_id"
  end

  add_index "histories", ["step_id"], name: "index_histories_on_step_id", using: :btree
  add_index "histories", ["user_id"], name: "index_histories_on_user_id", using: :btree

  create_table "history_files", force: :cascade do |t|
    t.string   "name"
    t.text     "file"
    t.integer  "history_id"
    t.string   "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "history_files", ["history_id"], name: "index_history_files_on_history_id", using: :btree

  create_table "import_files", force: :cascade do |t|
    t.text     "file"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "indicator_histories", force: :cascade do |t|
    t.integer  "indicator_id"
    t.date     "date"
    t.decimal  "value"
    t.string   "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "indicator_histories", ["indicator_id"], name: "index_indicator_histories_on_indicator_id", using: :btree

  create_table "indicators", force: :cascade do |t|
    t.string   "name"
    t.string   "type"
    t.text     "description"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "user_id"
    t.integer  "gestor_id"
    t.text     "identification"
    t.string   "icon"
    t.string   "color"
    t.string   "measure"
    t.boolean  "pdris"
    t.integer  "ord"
    t.decimal  "referencial"
    t.string   "referencial_measure"
    t.integer  "twitter_poll_id"
    t.boolean  "external"
    t.boolean  "contract"
    t.string   "department"
  end

  add_index "indicators", ["twitter_poll_id"], name: "index_indicators_on_twitter_poll_id", using: :btree
  add_index "indicators", ["user_id"], name: "index_indicators_on_user_id", using: :btree

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "link"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
  end

  add_index "news", ["user_id"], name: "index_news_on_user_id", using: :btree

  create_table "project_deeds", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "deed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  add_index "project_deeds", ["project_id"], name: "index_project_deeds_on_project_id", using: :btree

  create_table "project_users", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "project_users", ["project_id"], name: "index_project_users_on_project_id", using: :btree
  add_index "project_users", ["user_id"], name: "index_project_users_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.decimal  "budget"
    t.decimal  "committed"
    t.decimal  "paid"
    t.string   "status"
    t.text     "description"
    t.integer  "percent"
    t.date     "estimated_date"
    t.date     "finish_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.date     "start_date"
    t.integer  "project_id"
    t.integer  "indice_atualizacao"
    t.integer  "indice_pontualidade"
    t.integer  "indice_execucao"
    t.decimal  "lat"
    t.decimal  "lng"
    t.integer  "indicator_id"
    t.integer  "user_id"
    t.integer  "decision_id"
    t.integer  "delay"
    t.integer  "committal_id"
    t.decimal  "changes_amount"
    t.text     "live"
  end

  add_index "projects", ["committal_id"], name: "index_projects_on_committal_id", using: :btree
  add_index "projects", ["decision_id"], name: "index_projects_on_decision_id", using: :btree
  add_index "projects", ["indicator_id"], name: "index_projects_on_indicator_id", using: :btree
  add_index "projects", ["project_id"], name: "index_projects_on_project_id", using: :btree
  add_index "projects", ["user_id"], name: "index_projects_on_user_id", using: :btree

  create_table "question_replies", force: :cascade do |t|
    t.integer  "question_id"
    t.integer  "reply_id"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "question_replies", ["question_id"], name: "index_question_replies_on_question_id", using: :btree
  add_index "question_replies", ["reply_id"], name: "index_question_replies_on_reply_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "raes", force: :cascade do |t|
    t.date     "date"
    t.text     "ata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "raes", ["user_id"], name: "index_raes_on_user_id", using: :btree

  create_table "replies", force: :cascade do |t|
    t.integer  "indicator_id"
    t.integer  "user_id"
    t.text     "sumary"
    t.text     "suggestion"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.date     "start_date"
    t.date     "finish_date"
    t.text     "sumary_seplan"
    t.text     "suggestion_seplan"
    t.text     "pdris_sumary"
  end

  add_index "replies", ["indicator_id"], name: "index_replies_on_indicator_id", using: :btree
  add_index "replies", ["user_id"], name: "index_replies_on_user_id", using: :btree

  create_table "reply_committals", force: :cascade do |t|
    t.integer  "reply_id"
    t.integer  "committal_id"
    t.text     "text"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "reply_committals", ["committal_id"], name: "index_reply_committals_on_committal_id", using: :btree
  add_index "reply_committals", ["reply_id"], name: "index_reply_committals_on_reply_id", using: :btree

  create_table "statistic_histories", force: :cascade do |t|
    t.integer  "statistic_id"
    t.date     "date"
    t.decimal  "value"
    t.string   "caret"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "statistic_histories", ["statistic_id"], name: "index_statistic_histories_on_statistic_id", using: :btree

  create_table "statistics", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "analize"
    t.string   "measure"
    t.string   "color"
    t.string   "icon"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "group_statistic_id"
  end

  add_index "statistics", ["group_statistic_id"], name: "index_statistics_on_group_statistic_id", using: :btree

  create_table "steps", force: :cascade do |t|
    t.string   "name"
    t.date     "estimated_date"
    t.date     "finish_date"
    t.string   "status"
    t.text     "description"
    t.integer  "percent"
    t.integer  "project_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "days"
    t.date     "start_date"
  end

  add_index "steps", ["project_id"], name: "index_steps_on_project_id", using: :btree

  create_table "twitter_counts", force: :cascade do |t|
    t.integer  "twitter_poll_id"
    t.text     "text"
    t.integer  "value"
    t.string   "group_name"
    t.boolean  "good"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "last_tweet"
  end

  add_index "twitter_counts", ["twitter_poll_id"], name: "index_twitter_counts_on_twitter_poll_id", using: :btree

  create_table "twitter_polls", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "token"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "role"
    t.boolean  "gestor"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "year_histories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.date     "date"
    t.integer  "indicator_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "year_histories", ["indicator_id"], name: "index_year_histories_on_indicator_id", using: :btree
  add_index "year_histories", ["user_id"], name: "index_year_histories_on_user_id", using: :btree

  add_foreign_key "city_links", "cities"
  add_foreign_key "committal_evolutions", "committals"
  add_foreign_key "committals", "indicators"
  add_foreign_key "decisions", "raes"
  add_foreign_key "decisions", "users"
  add_foreign_key "goals", "indicators"
  add_foreign_key "histories", "steps"
  add_foreign_key "histories", "users"
  add_foreign_key "history_files", "histories"
  add_foreign_key "indicator_histories", "indicators"
  add_foreign_key "indicators", "twitter_polls"
  add_foreign_key "indicators", "users"
  add_foreign_key "news", "users"
  add_foreign_key "project_deeds", "projects"
  add_foreign_key "project_users", "projects"
  add_foreign_key "project_users", "users"
  add_foreign_key "projects", "committals"
  add_foreign_key "projects", "decisions"
  add_foreign_key "projects", "indicators"
  add_foreign_key "projects", "projects"
  add_foreign_key "projects", "users"
  add_foreign_key "question_replies", "questions"
  add_foreign_key "question_replies", "replies"
  add_foreign_key "raes", "users"
  add_foreign_key "replies", "indicators"
  add_foreign_key "replies", "users"
  add_foreign_key "reply_committals", "committals"
  add_foreign_key "reply_committals", "replies"
  add_foreign_key "statistic_histories", "statistics"
  add_foreign_key "statistics", "group_statistics"
  add_foreign_key "steps", "projects"
  add_foreign_key "twitter_counts", "twitter_polls"
  add_foreign_key "year_histories", "indicators"
  add_foreign_key "year_histories", "users"
end
