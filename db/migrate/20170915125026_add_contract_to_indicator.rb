class AddContractToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :contract, :boolean
    add_column :indicators, :department, :string
  end
end
