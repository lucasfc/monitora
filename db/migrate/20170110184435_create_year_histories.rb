class CreateYearHistories < ActiveRecord::Migration
  def change
    create_table :year_histories do |t|
      t.string :name
      t.text :description
      t.date :date
      t.references :indicator, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
