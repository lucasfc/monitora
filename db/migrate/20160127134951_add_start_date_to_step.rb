class AddStartDateToStep < ActiveRecord::Migration
  def change
    add_column :steps, :start_date, :date
  end
end
