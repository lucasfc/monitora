class AddGeocodeToProject < ActiveRecord::Migration
  def change
    add_column :projects, :lat, :decimal
    add_column :projects, :lng, :decimal
  end
end
