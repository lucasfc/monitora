class CreateReplyCommittals < ActiveRecord::Migration
  def change
    create_table :reply_committals do |t|
      t.references :reply, index: true, foreign_key: true
      t.references :committal, index: true, foreign_key: true
      t.text :text

      t.timestamps null: false
    end
  end
end
