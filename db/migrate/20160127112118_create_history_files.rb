class CreateHistoryFiles < ActiveRecord::Migration
  def change
    create_table :history_files do |t|
      t.string :name
      t.text :file
      t.references :history, index: true, foreign_key: true
      t.string :type

      t.timestamps null: false
    end
  end
end
