class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.references :indicator, index: true, foreign_key: true
      t.date :date
      t.decimal :value

      t.timestamps null: false
    end
  end
end
