class AddSumarySeplanToReply < ActiveRecord::Migration
  def change
    add_column :replies, :sumary_seplan, :text
    add_column :replies, :suggestion_seplan, :text
  end
end
