class CreateQuestionReplies < ActiveRecord::Migration
  def change
    create_table :question_replies do |t|
      t.references :question, index: true, foreign_key: true
      t.references :reply, index: true, foreign_key: true
      t.text :description

      t.timestamps null: false
    end
  end
end
