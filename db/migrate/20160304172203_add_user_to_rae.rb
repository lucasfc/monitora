class AddUserToRae < ActiveRecord::Migration
  def change
    add_reference :raes, :user, index: true, foreign_key: true
  end
end
