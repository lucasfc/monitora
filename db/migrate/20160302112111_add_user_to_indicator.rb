class AddUserToIndicator < ActiveRecord::Migration
  def change
    add_reference :indicators, :user, index: true, foreign_key: true
  end
end
