class CreateTwitterPolls < ActiveRecord::Migration
  def change
    create_table :twitter_polls do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
