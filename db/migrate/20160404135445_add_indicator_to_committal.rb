class AddIndicatorToCommittal < ActiveRecord::Migration
  def change
    add_reference :committals, :indicator, index: true, foreign_key: true
  end
end
