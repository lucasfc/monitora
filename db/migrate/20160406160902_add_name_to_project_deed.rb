class AddNameToProjectDeed < ActiveRecord::Migration
  def change
    add_column :project_deeds, :name, :string
  end
end
