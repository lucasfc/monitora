class CreateTwitterCounts < ActiveRecord::Migration
  def change
    create_table :twitter_counts do |t|
      t.references :twitter_poll, index: true, foreign_key: true
      t.text :text
      t.integer :value
      t.string :group_name
      t.boolean :good

      t.timestamps null: false
    end
  end
end
