class AddChangesAmountToProject < ActiveRecord::Migration
  def change
    add_column :projects, :changes_amount, :decimal
  end
end
