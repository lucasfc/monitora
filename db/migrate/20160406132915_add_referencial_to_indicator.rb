class AddReferencialToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :referencial, :decimal
    add_column :indicators, :referencial_measure, :string
  end
end
