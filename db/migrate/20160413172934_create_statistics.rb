class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.string :name
      t.text :description
      t.text :analize
      t.string :measure
      t.string :color
      t.string :icon

      t.timestamps null: false
    end
  end
end
