class CreateProjectDeeds < ActiveRecord::Migration
  def change
    create_table :project_deeds do |t|
      t.references :project, index: true, foreign_key: true
      t.integer :deed

      t.timestamps null: false
    end
  end
end
