class CreateGroupStatistics < ActiveRecord::Migration
  def change
    create_table :group_statistics do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
