class AddProbabilidadeToCommittal < ActiveRecord::Migration
  def change
    add_column :committals, :probabilidade, :integer
  end
end
