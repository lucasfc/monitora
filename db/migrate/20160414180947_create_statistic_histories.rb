class CreateStatisticHistories < ActiveRecord::Migration
  def change
    create_table :statistic_histories do |t|
      t.references :statistic, index: true, foreign_key: true
      t.date :date
      t.decimal :value
      t.string :caret

      t.timestamps null: false
    end
  end
end
