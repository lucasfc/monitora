class AddExecucaoToCommittal < ActiveRecord::Migration
  def change
    add_column :committals, :photo, :string
    add_column :committals, :start_date, :date
    add_column :committals, :estimate_date, :date
    add_column :committals, :orcamento, :decimal
    add_column :committals, :empenhado, :decimal
    add_column :committals, :liquidado, :decimal
    add_column :committals, :executado_financeiro, :decimal
    add_column :committals, :percentual_contratado, :decimal
    add_column :committals, :executado_vs_contratado, :decimal
    add_column :committals, :saldo, :decimal
    add_column :committals, :executado_fisico, :decimal
  end
end
