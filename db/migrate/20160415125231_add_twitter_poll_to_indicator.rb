class AddTwitterPollToIndicator < ActiveRecord::Migration
  def change
    add_reference :indicators, :twitter_poll, index: true, foreign_key: true
  end
end
