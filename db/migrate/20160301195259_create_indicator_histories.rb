class CreateIndicatorHistories < ActiveRecord::Migration
  def change
    create_table :indicator_histories do |t|
      t.references :indicator, index: true, foreign_key: true
      t.date :date
      t.decimal :value
      t.string :status

      t.timestamps null: false
    end
  end
end
