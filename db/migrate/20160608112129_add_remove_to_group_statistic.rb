class AddRemoveToGroupStatistic < ActiveRecord::Migration
  def change
    add_column :group_statistics, :remove, :boolean
  end
end
