class AddExternalToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :external, :boolean
  end
end
