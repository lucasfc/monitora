class CreateCityLinks < ActiveRecord::Migration
  def change
    create_table :city_links do |t|
      t.string :category
      t.text :link

      t.timestamps null: false
    end
  end
end
