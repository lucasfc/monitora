class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.decimal :budget
      t.decimal :committed
      t.decimal :paid
      t.string :status
      t.text :description
      t.integer :percent
      t.date :estimated_date
      t.date :finish_date

      t.timestamps null: false
    end
  end
end
