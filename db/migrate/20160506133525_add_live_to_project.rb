class AddLiveToProject < ActiveRecord::Migration
  def change
    add_column :projects, :live, :text
  end
end
