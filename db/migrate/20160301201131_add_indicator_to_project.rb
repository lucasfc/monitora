class AddIndicatorToProject < ActiveRecord::Migration
  def change
    add_reference :projects, :indicator, index: true, foreign_key: true
    add_reference :projects, :user, index: true, foreign_key: true
  end
end
