class AddGroupStatisticToStatistic < ActiveRecord::Migration
  def change
    add_reference :statistics, :group_statistic, index: true, foreign_key: true
  end
end
