class AddLastTweetToTwitterCount < ActiveRecord::Migration
  def change
    add_column :twitter_counts, :last_tweet, :string
  end
end
