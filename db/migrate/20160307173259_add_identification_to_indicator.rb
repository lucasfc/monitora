class AddIdentificationToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :identification, :text
  end
end
