class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.references :indicator, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :sumary
      t.text :suggestion

      t.timestamps null: false
    end
  end
end
