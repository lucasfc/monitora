class CreateHistories < ActiveRecord::Migration
  def change
    create_table :histories do |t|
      t.string :name
      t.text :description
      t.references :user, index: true, foreign_key: true
      t.boolean :finish

      t.timestamps null: false
    end
  end
end
