class CreateImportFiles < ActiveRecord::Migration
  def change
    create_table :import_files do |t|
      t.text :file
      t.string :category

      t.timestamps null: false
    end
  end
end
