class CreateDecisions < ActiveRecord::Migration
  def change
    create_table :decisions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :rae, index: true, foreign_key: true
      t.text :description
      t.string :status
      t.date :date

      t.timestamps null: false
    end
  end
end
