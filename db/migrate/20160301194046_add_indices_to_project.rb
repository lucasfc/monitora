class AddIndicesToProject < ActiveRecord::Migration
  def change
    add_column :projects, :indice_atualizacao, :integer
    add_column :projects, :indice_pontualidade, :integer
    add_column :projects, :indice_execucao, :integer
  end
end
