class AddChangesAmountToCommittal < ActiveRecord::Migration
  def change
    add_column :committals, :changes_amount, :decimal
  end
end
