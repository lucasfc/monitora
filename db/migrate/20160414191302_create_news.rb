class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.string :description
      t.string :link
      t.string :status

      t.timestamps null: false
    end
  end
end
