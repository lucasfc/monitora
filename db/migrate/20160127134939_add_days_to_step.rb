class AddDaysToStep < ActiveRecord::Migration
  def change
    add_column :steps, :days, :integer
  end
end
