class AddDelayToProject < ActiveRecord::Migration
  def change
    add_column :projects, :delay, :integer
  end
end
