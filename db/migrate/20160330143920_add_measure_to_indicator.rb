class AddMeasureToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :measure, :string
  end
end
