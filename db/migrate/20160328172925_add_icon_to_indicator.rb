class AddIconToIndicator < ActiveRecord::Migration
  def change
    add_column :indicators, :icon, :string
    add_column :indicators, :color, :string
  end
end
