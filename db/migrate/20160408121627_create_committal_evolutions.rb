class CreateCommittalEvolutions < ActiveRecord::Migration
  def change
    create_table :committal_evolutions do |t|
      t.references :committal, index: true, foreign_key: true
      t.decimal :orcamento
      t.decimal :empenhado
      t.decimal :liquidado
      t.decimal :executado_financeiro
      t.decimal :percentual_contratado
      t.decimal :executado_vs_contratado
      t.decimal :saldo
      t.decimal :executado_fisico

      t.timestamps null: false
    end
  end
end
