class AddDateRangeToReply < ActiveRecord::Migration
  def change
    add_column :replies, :start_date, :date
    add_column :replies, :finish_date, :date
  end
end
