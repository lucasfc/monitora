class AddStatusToHistory < ActiveRecord::Migration
  def change
    add_column :histories, :status, :string
    add_reference :histories, :step, index: true, foreign_key: true
  end
end
