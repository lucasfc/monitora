class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.string :name
      t.date :estimated_date
      t.date :finish_date
      t.string :status
      t.text :description
      t.integer :percent
      t.references :project, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
