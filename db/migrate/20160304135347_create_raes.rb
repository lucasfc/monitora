class CreateRaes < ActiveRecord::Migration
  def change
    create_table :raes do |t|
      t.date :date
      t.text :ata

      t.timestamps null: false
    end
  end
end
