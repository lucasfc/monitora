class AddIconToGroupStatistic < ActiveRecord::Migration
  def change
    add_column :group_statistics, :icon, :string
    add_column :group_statistics, :color, :string
  end
end
