class AddCityToCityLink < ActiveRecord::Migration
  def change
    add_reference :city_links, :city, index: true, foreign_key: true
  end
end
