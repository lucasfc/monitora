class AddDecisionToProject < ActiveRecord::Migration
  def change
    add_reference :projects, :decision, index: true, foreign_key: true
  end
end
