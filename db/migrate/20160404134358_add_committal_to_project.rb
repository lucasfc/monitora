class AddCommittalToProject < ActiveRecord::Migration
  def change
    add_reference :projects, :committal, index: true, foreign_key: true
  end
end
