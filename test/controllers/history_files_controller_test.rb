require 'test_helper'

class HistoryFilesControllerTest < ActionController::TestCase
  setup do
    @history_file = history_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:history_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create history_file" do
    assert_difference('HistoryFile.count') do
      post :create, history_file: { file: @history_file.file, history_id: @history_file.history_id, name: @history_file.name, type: @history_file.type }
    end

    assert_redirected_to history_file_path(assigns(:history_file))
  end

  test "should show history_file" do
    get :show, id: @history_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @history_file
    assert_response :success
  end

  test "should update history_file" do
    patch :update, id: @history_file, history_file: { file: @history_file.file, history_id: @history_file.history_id, name: @history_file.name, type: @history_file.type }
    assert_redirected_to history_file_path(assigns(:history_file))
  end

  test "should destroy history_file" do
    assert_difference('HistoryFile.count', -1) do
      delete :destroy, id: @history_file
    end

    assert_redirected_to history_files_path
  end
end
