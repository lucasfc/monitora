Rails.application.routes.draw do

  get 'economia/index'
  get 'economia/ranking'

  get 'economia/subitem', as: :economia_subitem

  get 'economia/sgd'

  get 'saude/index'

  get 'saude/medicamentos'

  get 'saude/estoque'

  get 'saude/cirurgias_eletivas'

  get 'saude/ocupacao'

  resources :city_links
  resources :cities
  resources :year_histories
  get 'receita/avaliacao', as: :receita_avaliacao

  get 'receita/relatorio'

  get 'despesa/avaliacao', as: :despesa_avaliacao

  get 'despesa/relatorio', as: :despesa_relatorio
  get 'despesa/pdris', as: :despesa_pdris
  get 'despesa/pessoal', as: :despesa_pessoal
  get 'despesa/comparativo', as: :despesa_comparativo
  post 'despesa/comparativo', as: :despesa_comparativo_post
  get 'despesa/natureza', as: :despesa_natureza
  post 'despesa/natureza', as: :despesa_natureza_post
  get 'despesa/dea', as: :despesa_dea
  post 'despesa/dea', as: :despesa_dea_post
  get 'despesa/insuficiencia', as: :despesa_insuficiencia
  post 'despesa/insuficiencia', as: :despesa_insuficiencia_post
  get 'despesa/balanco', as: :despesa_balanco

  get 'convenios/index', as: :convenios

  get 'convenios/filter', as: :convenios_filter

  get 'emendas/index', as: :emendas

  get 'emendas/resume', as: :emendas_resume

   get 'emendas/home', as: :emendas_home

  resources :import_files
  resources :twitter_counts
  resources :twitter_polls
  resources :news do
    member do
      get "news_status"
    end
    collection do
      get "aprovar"
    end
  end
  resources :statistic_histories
  resources :statistics
  resources :group_statistics
  resources :committal_evolutions
  resources :committals do
    member do
      get 'projects'
    end
  end

  get 'pdris' => "pdris#index", as: :pdris

  get 'pdris/:id' => "pdris#show", as: :pdris_item

  get 'home/index'
  get 'home/gestores'
  get 'home/imprensa', as: :imprensa
  get 'home/redes_sociais', as: :redes_sociais
  get 'home/informacao', as: :informacao
  get 'home/orcamento', as: :orcamento_menu
  get 'home/receita', as: :receita_menu
  get 'home/despesa', as: :despesa_menu
  get 'home/lrf', as: :lrf
  get 'home/resultados', as: :resultados
  get 'home/tocantins_em_numeros', as: :tocantins_em_numeros
  get 'home/cidades', as: :cidades

  get 'powerbi/orcamento', as: :orcamento_bi
  get 'powerbi/receita', as: :receita_bi
  get 'powerbi/evolucao'
  get 'powerbi/mobile_geral', as: :mobile_geral
  get 'powerbi/fonte/:fonte' => "powerbi#fonte", as: :fonte_bi
  get 'powerbi/search' => "powerbi#search", as: :search_bi
  get 'powerbi/search_result' => "powerbi#search_result", as: :search_result_bi
  get 'powerbi/receita_result' => "powerbi#receita_result", as: :receita_result_bi
  get 'powerbi/receita_evolution' => "powerbi#receita_evolution", as: :receita_evolution_bi

  resources :decisions do
    member do
      get "create_project"
    end
  end
  
  resources :raes
  resources :question_replies
  resources :replies
  resources :questions
  resources :goals
  resources :indicator_histories
  resources :indicators do
    member do
      get "analyze"
      get "goals"
      get "histories"
      get "entregas_combo"
      get "probabilidade"
      get "year_history"
    end
    collection do
      get "report"
    end
  end
  get 'auth/redirect' => 'toauth#redirect'
  get 'toauth' => 'toauth#entrar'
  get '/users/sign_in' => 'toauth#redirect'
  devise_for :users
  resources :users
  resources :history_files
  resources :steps
  resources :histories
  resources :projects do
    collection do
      get "my"
    end
    member do
      get "orcamento"
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  authenticated :user do
      root 'home#gestores', as: :home_gestor
  end
  constraints DomainCheck.new('www.monitora.to.gov.br') do
    root :to => 'publico#index'
  end

  get 'deed_evaluations/:deed_id' => 'indicators#deed_evaluation'
  get 'deed_acompanhamento/:deed_id' => 'indicators#deed_acompanhamento'

  root 'indicators#index', as: :home_all

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end


  #Público
    get 'publico/index', as: :publico
    get 'publico/orcamento', as: :pub_orcamento
    get 'publico/entregas', as: :pub_entregas
    get 'publico/pdris', as: :pub_pdris
    get 'publico/indicadores', as: :pub_indicadores
    get 'publico/despesa', as: :pub_despesa
    get 'publico/widget', as: :pub_widget
    get 'publico/secretario', as: :pub_secretario
    get 'publico/secretario_pre', as: :pub_secretario_pre
end
