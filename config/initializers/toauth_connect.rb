ToauthConnect.configuration do |config|
  config.client_id  = "e00f26aae1e05f14b951022d51edd3d64e2b8ac6aee65963b33ad2edb9fc15b1" #ID de Cliente gerado no Toauth
  config.client_secret  = "220a158fc87fe6c2113af5bc2e49aa7f9217227ef7fb0a709be1ee8e2432bc73" #Chave secreta gerada no Toauth
  config.domain_default  = "http://monitora.seplan.to.gov.br/" #Domnio padrão
  config.url_redirect_production  = "http://monitora.seplan.to.gov.br/toauth" #URL de Redirecionamento em produção
  config.url_redirect_dev  = "http://localhost:3000/toauth" #Url de redirecionamento em desenvolvimento
  config.user_table  = "User" #Nome da tabela contendo o Usuário
  config.column_email  = "email" #Coluna da tabela contendo o email
  config.column_token  = "token" #Coluna da tabela contendo o token
  config.column_name  = "name" #Coluna da tabela contendo o nome
  config.column_cpf  = "cpf" #Coluna da tabela contendo o cpf
  config.column_password  = "password" #Coluna da tabela contendo o password
  config.column_password_confirmation  = "password_confirmation" #Coluna da tabela contendo a confirmação de password
end