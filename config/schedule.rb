every 1.hours do
  rake "update_projects:update_steps"
  rake "update_projects:update_indices"
  rake "social:refresh_counters"
end

every '0 0 1 * *' do
  rake "committals:save_history"
end