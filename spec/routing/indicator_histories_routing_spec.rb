require "rails_helper"

RSpec.describe IndicatorHistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/indicator_histories").to route_to("indicator_histories#index")
    end

    it "routes to #new" do
      expect(:get => "/indicator_histories/new").to route_to("indicator_histories#new")
    end

    it "routes to #show" do
      expect(:get => "/indicator_histories/1").to route_to("indicator_histories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/indicator_histories/1/edit").to route_to("indicator_histories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/indicator_histories").to route_to("indicator_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/indicator_histories/1").to route_to("indicator_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/indicator_histories/1").to route_to("indicator_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/indicator_histories/1").to route_to("indicator_histories#destroy", :id => "1")
    end

  end
end
