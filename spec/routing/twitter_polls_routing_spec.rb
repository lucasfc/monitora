require "rails_helper"

RSpec.describe TwitterPollsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/twitter_polls").to route_to("twitter_polls#index")
    end

    it "routes to #new" do
      expect(:get => "/twitter_polls/new").to route_to("twitter_polls#new")
    end

    it "routes to #show" do
      expect(:get => "/twitter_polls/1").to route_to("twitter_polls#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/twitter_polls/1/edit").to route_to("twitter_polls#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/twitter_polls").to route_to("twitter_polls#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/twitter_polls/1").to route_to("twitter_polls#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/twitter_polls/1").to route_to("twitter_polls#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/twitter_polls/1").to route_to("twitter_polls#destroy", :id => "1")
    end

  end
end
