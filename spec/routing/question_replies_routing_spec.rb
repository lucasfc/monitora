require "rails_helper"

RSpec.describe QuestionRepliesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/question_replies").to route_to("question_replies#index")
    end

    it "routes to #new" do
      expect(:get => "/question_replies/new").to route_to("question_replies#new")
    end

    it "routes to #show" do
      expect(:get => "/question_replies/1").to route_to("question_replies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/question_replies/1/edit").to route_to("question_replies#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/question_replies").to route_to("question_replies#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/question_replies/1").to route_to("question_replies#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/question_replies/1").to route_to("question_replies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/question_replies/1").to route_to("question_replies#destroy", :id => "1")
    end

  end
end
