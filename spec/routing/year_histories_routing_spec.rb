require "rails_helper"

RSpec.describe YearHistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/year_histories").to route_to("year_histories#index")
    end

    it "routes to #new" do
      expect(:get => "/year_histories/new").to route_to("year_histories#new")
    end

    it "routes to #show" do
      expect(:get => "/year_histories/1").to route_to("year_histories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/year_histories/1/edit").to route_to("year_histories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/year_histories").to route_to("year_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/year_histories/1").to route_to("year_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/year_histories/1").to route_to("year_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/year_histories/1").to route_to("year_histories#destroy", :id => "1")
    end

  end
end
