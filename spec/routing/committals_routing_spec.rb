require "rails_helper"

RSpec.describe CommittalsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/committals").to route_to("committals#index")
    end

    it "routes to #new" do
      expect(:get => "/committals/new").to route_to("committals#new")
    end

    it "routes to #show" do
      expect(:get => "/committals/1").to route_to("committals#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/committals/1/edit").to route_to("committals#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/committals").to route_to("committals#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/committals/1").to route_to("committals#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/committals/1").to route_to("committals#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/committals/1").to route_to("committals#destroy", :id => "1")
    end

  end
end
