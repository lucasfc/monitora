require "rails_helper"

RSpec.describe CommittalEvolutionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/committal_evolutions").to route_to("committal_evolutions#index")
    end

    it "routes to #new" do
      expect(:get => "/committal_evolutions/new").to route_to("committal_evolutions#new")
    end

    it "routes to #show" do
      expect(:get => "/committal_evolutions/1").to route_to("committal_evolutions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/committal_evolutions/1/edit").to route_to("committal_evolutions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/committal_evolutions").to route_to("committal_evolutions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/committal_evolutions/1").to route_to("committal_evolutions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/committal_evolutions/1").to route_to("committal_evolutions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/committal_evolutions/1").to route_to("committal_evolutions#destroy", :id => "1")
    end

  end
end
