require "rails_helper"

RSpec.describe StatisticHistoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/statistic_histories").to route_to("statistic_histories#index")
    end

    it "routes to #new" do
      expect(:get => "/statistic_histories/new").to route_to("statistic_histories#new")
    end

    it "routes to #show" do
      expect(:get => "/statistic_histories/1").to route_to("statistic_histories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/statistic_histories/1/edit").to route_to("statistic_histories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/statistic_histories").to route_to("statistic_histories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/statistic_histories/1").to route_to("statistic_histories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/statistic_histories/1").to route_to("statistic_histories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/statistic_histories/1").to route_to("statistic_histories#destroy", :id => "1")
    end

  end
end
