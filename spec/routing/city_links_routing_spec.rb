require "rails_helper"

RSpec.describe CityLinksController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/city_links").to route_to("city_links#index")
    end

    it "routes to #new" do
      expect(:get => "/city_links/new").to route_to("city_links#new")
    end

    it "routes to #show" do
      expect(:get => "/city_links/1").to route_to("city_links#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/city_links/1/edit").to route_to("city_links#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/city_links").to route_to("city_links#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/city_links/1").to route_to("city_links#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/city_links/1").to route_to("city_links#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/city_links/1").to route_to("city_links#destroy", :id => "1")
    end

  end
end
