require "rails_helper"

RSpec.describe GroupStatisticsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/group_statistics").to route_to("group_statistics#index")
    end

    it "routes to #new" do
      expect(:get => "/group_statistics/new").to route_to("group_statistics#new")
    end

    it "routes to #show" do
      expect(:get => "/group_statistics/1").to route_to("group_statistics#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/group_statistics/1/edit").to route_to("group_statistics#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/group_statistics").to route_to("group_statistics#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/group_statistics/1").to route_to("group_statistics#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/group_statistics/1").to route_to("group_statistics#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/group_statistics/1").to route_to("group_statistics#destroy", :id => "1")
    end

  end
end
