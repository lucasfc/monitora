require "rails_helper"

RSpec.describe ImportFilesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/import_files").to route_to("import_files#index")
    end

    it "routes to #new" do
      expect(:get => "/import_files/new").to route_to("import_files#new")
    end

    it "routes to #show" do
      expect(:get => "/import_files/1").to route_to("import_files#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/import_files/1/edit").to route_to("import_files#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/import_files").to route_to("import_files#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/import_files/1").to route_to("import_files#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/import_files/1").to route_to("import_files#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/import_files/1").to route_to("import_files#destroy", :id => "1")
    end

  end
end
