require "rails_helper"

RSpec.describe TwitterCountsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/twitter_counts").to route_to("twitter_counts#index")
    end

    it "routes to #new" do
      expect(:get => "/twitter_counts/new").to route_to("twitter_counts#new")
    end

    it "routes to #show" do
      expect(:get => "/twitter_counts/1").to route_to("twitter_counts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/twitter_counts/1/edit").to route_to("twitter_counts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/twitter_counts").to route_to("twitter_counts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/twitter_counts/1").to route_to("twitter_counts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/twitter_counts/1").to route_to("twitter_counts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/twitter_counts/1").to route_to("twitter_counts#destroy", :id => "1")
    end

  end
end
