require 'rails_helper'

RSpec.describe "goals/new", type: :view do
  before(:each) do
    assign(:goal, Goal.new(
      :indicator => nil,
      :value => "9.99"
    ))
  end

  it "renders new goal form" do
    render

    assert_select "form[action=?][method=?]", goals_path, "post" do

      assert_select "input#goal_indicator_id[name=?]", "goal[indicator_id]"

      assert_select "input#goal_value[name=?]", "goal[value]"
    end
  end
end
