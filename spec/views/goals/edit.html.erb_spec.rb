require 'rails_helper'

RSpec.describe "goals/edit", type: :view do
  before(:each) do
    @goal = assign(:goal, Goal.create!(
      :indicator => nil,
      :value => "9.99"
    ))
  end

  it "renders the edit goal form" do
    render

    assert_select "form[action=?][method=?]", goal_path(@goal), "post" do

      assert_select "input#goal_indicator_id[name=?]", "goal[indicator_id]"

      assert_select "input#goal_value[name=?]", "goal[value]"
    end
  end
end
