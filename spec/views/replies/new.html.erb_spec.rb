require 'rails_helper'

RSpec.describe "replies/new", type: :view do
  before(:each) do
    assign(:reply, Reply.new(
      :indicator => nil,
      :user => nil,
      :sumary => "MyText",
      :suggestion => "MyText"
    ))
  end

  it "renders new reply form" do
    render

    assert_select "form[action=?][method=?]", replies_path, "post" do

      assert_select "input#reply_indicator_id[name=?]", "reply[indicator_id]"

      assert_select "input#reply_user_id[name=?]", "reply[user_id]"

      assert_select "textarea#reply_sumary[name=?]", "reply[sumary]"

      assert_select "textarea#reply_suggestion[name=?]", "reply[suggestion]"
    end
  end
end
