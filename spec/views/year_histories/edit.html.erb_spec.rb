require 'rails_helper'

RSpec.describe "year_histories/edit", type: :view do
  before(:each) do
    @year_history = assign(:year_history, YearHistory.create!(
      :name => "MyString",
      :description => "MyText",
      :indicator => nil,
      :user => nil
    ))
  end

  it "renders the edit year_history form" do
    render

    assert_select "form[action=?][method=?]", year_history_path(@year_history), "post" do

      assert_select "input#year_history_name[name=?]", "year_history[name]"

      assert_select "textarea#year_history_description[name=?]", "year_history[description]"

      assert_select "input#year_history_indicator_id[name=?]", "year_history[indicator_id]"

      assert_select "input#year_history_user_id[name=?]", "year_history[user_id]"
    end
  end
end
