require 'rails_helper'

RSpec.describe "year_histories/show", type: :view do
  before(:each) do
    @year_history = assign(:year_history, YearHistory.create!(
      :name => "Name",
      :description => "MyText",
      :indicator => nil,
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
