require 'rails_helper'

RSpec.describe "year_histories/index", type: :view do
  before(:each) do
    assign(:year_histories, [
      YearHistory.create!(
        :name => "Name",
        :description => "MyText",
        :indicator => nil,
        :user => nil
      ),
      YearHistory.create!(
        :name => "Name",
        :description => "MyText",
        :indicator => nil,
        :user => nil
      )
    ])
  end

  it "renders a list of year_histories" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
