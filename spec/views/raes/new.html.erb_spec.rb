require 'rails_helper'

RSpec.describe "raes/new", type: :view do
  before(:each) do
    assign(:rae, Rae.new(
      :ata => "MyText"
    ))
  end

  it "renders new rae form" do
    render

    assert_select "form[action=?][method=?]", raes_path, "post" do

      assert_select "textarea#rae_ata[name=?]", "rae[ata]"
    end
  end
end
