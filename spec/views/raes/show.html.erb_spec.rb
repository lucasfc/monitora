require 'rails_helper'

RSpec.describe "raes/show", type: :view do
  before(:each) do
    @rae = assign(:rae, Rae.create!(
      :ata => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
  end
end
