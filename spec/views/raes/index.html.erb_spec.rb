require 'rails_helper'

RSpec.describe "raes/index", type: :view do
  before(:each) do
    assign(:raes, [
      Rae.create!(
        :ata => "MyText"
      ),
      Rae.create!(
        :ata => "MyText"
      )
    ])
  end

  it "renders a list of raes" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
