require 'rails_helper'

RSpec.describe "raes/edit", type: :view do
  before(:each) do
    @rae = assign(:rae, Rae.create!(
      :ata => "MyText"
    ))
  end

  it "renders the edit rae form" do
    render

    assert_select "form[action=?][method=?]", rae_path(@rae), "post" do

      assert_select "textarea#rae_ata[name=?]", "rae[ata]"
    end
  end
end
