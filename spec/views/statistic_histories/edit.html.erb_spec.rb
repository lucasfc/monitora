require 'rails_helper'

RSpec.describe "statistic_histories/edit", type: :view do
  before(:each) do
    @statistic_history = assign(:statistic_history, StatisticHistory.create!(
      :statistic => nil,
      :value => "9.99",
      :caret => "MyString"
    ))
  end

  it "renders the edit statistic_history form" do
    render

    assert_select "form[action=?][method=?]", statistic_history_path(@statistic_history), "post" do

      assert_select "input#statistic_history_statistic_id[name=?]", "statistic_history[statistic_id]"

      assert_select "input#statistic_history_value[name=?]", "statistic_history[value]"

      assert_select "input#statistic_history_caret[name=?]", "statistic_history[caret]"
    end
  end
end
