require 'rails_helper'

RSpec.describe "statistic_histories/index", type: :view do
  before(:each) do
    assign(:statistic_histories, [
      StatisticHistory.create!(
        :statistic => nil,
        :value => "9.99",
        :caret => "Caret"
      ),
      StatisticHistory.create!(
        :statistic => nil,
        :value => "9.99",
        :caret => "Caret"
      )
    ])
  end

  it "renders a list of statistic_histories" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Caret".to_s, :count => 2
  end
end
