require 'rails_helper'

RSpec.describe "statistic_histories/show", type: :view do
  before(:each) do
    @statistic_history = assign(:statistic_history, StatisticHistory.create!(
      :statistic => nil,
      :value => "9.99",
      :caret => "Caret"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Caret/)
  end
end
