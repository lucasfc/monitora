require 'rails_helper'

RSpec.describe "statistic_histories/new", type: :view do
  before(:each) do
    assign(:statistic_history, StatisticHistory.new(
      :statistic => nil,
      :value => "9.99",
      :caret => "MyString"
    ))
  end

  it "renders new statistic_history form" do
    render

    assert_select "form[action=?][method=?]", statistic_histories_path, "post" do

      assert_select "input#statistic_history_statistic_id[name=?]", "statistic_history[statistic_id]"

      assert_select "input#statistic_history_value[name=?]", "statistic_history[value]"

      assert_select "input#statistic_history_caret[name=?]", "statistic_history[caret]"
    end
  end
end
