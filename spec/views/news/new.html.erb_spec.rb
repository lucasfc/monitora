require 'rails_helper'

RSpec.describe "news/new", type: :view do
  before(:each) do
    assign(:news, News.new(
      :title => "MyString",
      :description => "MyString",
      :link => "MyString",
      :status => "MyString"
    ))
  end

  it "renders new news form" do
    render

    assert_select "form[action=?][method=?]", news_index_path, "post" do

      assert_select "input#news_title[name=?]", "news[title]"

      assert_select "input#news_description[name=?]", "news[description]"

      assert_select "input#news_link[name=?]", "news[link]"

      assert_select "input#news_status[name=?]", "news[status]"
    end
  end
end
