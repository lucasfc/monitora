require 'rails_helper'

RSpec.describe "news/index", type: :view do
  before(:each) do
    assign(:news, [
      News.create!(
        :title => "Title",
        :description => "Description",
        :link => "Link",
        :status => "Status"
      ),
      News.create!(
        :title => "Title",
        :description => "Description",
        :link => "Link",
        :status => "Status"
      )
    ])
  end

  it "renders a list of news" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => "Link".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
