require 'rails_helper'

RSpec.describe "question_replies/index", type: :view do
  before(:each) do
    assign(:question_replies, [
      QuestionReply.create!(
        :question => nil,
        :reply => nil,
        :description => "MyText"
      ),
      QuestionReply.create!(
        :question => nil,
        :reply => nil,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of question_replies" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
