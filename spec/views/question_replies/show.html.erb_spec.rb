require 'rails_helper'

RSpec.describe "question_replies/show", type: :view do
  before(:each) do
    @question_reply = assign(:question_reply, QuestionReply.create!(
      :question => nil,
      :reply => nil,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
  end
end
