require 'rails_helper'

RSpec.describe "question_replies/new", type: :view do
  before(:each) do
    assign(:question_reply, QuestionReply.new(
      :question => nil,
      :reply => nil,
      :description => "MyText"
    ))
  end

  it "renders new question_reply form" do
    render

    assert_select "form[action=?][method=?]", question_replies_path, "post" do

      assert_select "input#question_reply_question_id[name=?]", "question_reply[question_id]"

      assert_select "input#question_reply_reply_id[name=?]", "question_reply[reply_id]"

      assert_select "textarea#question_reply_description[name=?]", "question_reply[description]"
    end
  end
end
