require 'rails_helper'

RSpec.describe "question_replies/edit", type: :view do
  before(:each) do
    @question_reply = assign(:question_reply, QuestionReply.create!(
      :question => nil,
      :reply => nil,
      :description => "MyText"
    ))
  end

  it "renders the edit question_reply form" do
    render

    assert_select "form[action=?][method=?]", question_reply_path(@question_reply), "post" do

      assert_select "input#question_reply_question_id[name=?]", "question_reply[question_id]"

      assert_select "input#question_reply_reply_id[name=?]", "question_reply[reply_id]"

      assert_select "textarea#question_reply_description[name=?]", "question_reply[description]"
    end
  end
end
