require 'rails_helper'

RSpec.describe "indicators/index", type: :view do
  before(:each) do
    assign(:indicators, [
      Indicator.create!(
        :name => "Name",
        :type => "Type",
        :description => "MyText"
      ),
      Indicator.create!(
        :name => "Name",
        :type => "Type",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of indicators" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Type".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
