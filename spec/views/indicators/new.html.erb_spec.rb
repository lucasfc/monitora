require 'rails_helper'

RSpec.describe "indicators/new", type: :view do
  before(:each) do
    assign(:indicator, Indicator.new(
      :name => "MyString",
      :type => "",
      :description => "MyText"
    ))
  end

  it "renders new indicator form" do
    render

    assert_select "form[action=?][method=?]", indicators_path, "post" do

      assert_select "input#indicator_name[name=?]", "indicator[name]"

      assert_select "input#indicator_type[name=?]", "indicator[type]"

      assert_select "textarea#indicator_description[name=?]", "indicator[description]"
    end
  end
end
