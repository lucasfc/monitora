require 'rails_helper'

RSpec.describe "indicators/edit", type: :view do
  before(:each) do
    @indicator = assign(:indicator, Indicator.create!(
      :name => "MyString",
      :type => "",
      :description => "MyText"
    ))
  end

  it "renders the edit indicator form" do
    render

    assert_select "form[action=?][method=?]", indicator_path(@indicator), "post" do

      assert_select "input#indicator_name[name=?]", "indicator[name]"

      assert_select "input#indicator_type[name=?]", "indicator[type]"

      assert_select "textarea#indicator_description[name=?]", "indicator[description]"
    end
  end
end
