require 'rails_helper'

RSpec.describe "statistics/new", type: :view do
  before(:each) do
    assign(:statistic, Statistic.new(
      :name => "MyString",
      :description => "MyText",
      :analize => "MyText",
      :group_statistcs => nil,
      :measure => "MyString",
      :color => "MyString",
      :icon => "MyString"
    ))
  end

  it "renders new statistic form" do
    render

    assert_select "form[action=?][method=?]", statistics_path, "post" do

      assert_select "input#statistic_name[name=?]", "statistic[name]"

      assert_select "textarea#statistic_description[name=?]", "statistic[description]"

      assert_select "textarea#statistic_analize[name=?]", "statistic[analize]"

      assert_select "input#statistic_group_statistcs_id[name=?]", "statistic[group_statistcs_id]"

      assert_select "input#statistic_measure[name=?]", "statistic[measure]"

      assert_select "input#statistic_color[name=?]", "statistic[color]"

      assert_select "input#statistic_icon[name=?]", "statistic[icon]"
    end
  end
end
