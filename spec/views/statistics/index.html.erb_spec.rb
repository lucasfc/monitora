require 'rails_helper'

RSpec.describe "statistics/index", type: :view do
  before(:each) do
    assign(:statistics, [
      Statistic.create!(
        :name => "Name",
        :description => "MyText",
        :analize => "MyText",
        :group_statistcs => nil,
        :measure => "Measure",
        :color => "Color",
        :icon => "Icon"
      ),
      Statistic.create!(
        :name => "Name",
        :description => "MyText",
        :analize => "MyText",
        :group_statistcs => nil,
        :measure => "Measure",
        :color => "Color",
        :icon => "Icon"
      )
    ])
  end

  it "renders a list of statistics" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Measure".to_s, :count => 2
    assert_select "tr>td", :text => "Color".to_s, :count => 2
    assert_select "tr>td", :text => "Icon".to_s, :count => 2
  end
end
