require 'rails_helper'

RSpec.describe "statistics/show", type: :view do
  before(:each) do
    @statistic = assign(:statistic, Statistic.create!(
      :name => "Name",
      :description => "MyText",
      :analize => "MyText",
      :group_statistcs => nil,
      :measure => "Measure",
      :color => "Color",
      :icon => "Icon"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(//)
    expect(rendered).to match(/Measure/)
    expect(rendered).to match(/Color/)
    expect(rendered).to match(/Icon/)
  end
end
