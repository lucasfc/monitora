require 'rails_helper'

RSpec.describe "city_links/new", type: :view do
  before(:each) do
    assign(:city_link, CityLink.new(
      :category => "MyString",
      :link => "MyText"
    ))
  end

  it "renders new city_link form" do
    render

    assert_select "form[action=?][method=?]", city_links_path, "post" do

      assert_select "input#city_link_category[name=?]", "city_link[category]"

      assert_select "textarea#city_link_link[name=?]", "city_link[link]"
    end
  end
end
