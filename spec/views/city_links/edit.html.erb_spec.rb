require 'rails_helper'

RSpec.describe "city_links/edit", type: :view do
  before(:each) do
    @city_link = assign(:city_link, CityLink.create!(
      :category => "MyString",
      :link => "MyText"
    ))
  end

  it "renders the edit city_link form" do
    render

    assert_select "form[action=?][method=?]", city_link_path(@city_link), "post" do

      assert_select "input#city_link_category[name=?]", "city_link[category]"

      assert_select "textarea#city_link_link[name=?]", "city_link[link]"
    end
  end
end
