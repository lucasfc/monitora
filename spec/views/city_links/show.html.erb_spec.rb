require 'rails_helper'

RSpec.describe "city_links/show", type: :view do
  before(:each) do
    @city_link = assign(:city_link, CityLink.create!(
      :category => "Category",
      :link => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Category/)
    expect(rendered).to match(/MyText/)
  end
end
