require 'rails_helper'

RSpec.describe "city_links/index", type: :view do
  before(:each) do
    assign(:city_links, [
      CityLink.create!(
        :category => "Category",
        :link => "MyText"
      ),
      CityLink.create!(
        :category => "Category",
        :link => "MyText"
      )
    ])
  end

  it "renders a list of city_links" do
    render
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
