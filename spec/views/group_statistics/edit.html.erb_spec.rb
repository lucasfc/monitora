require 'rails_helper'

RSpec.describe "group_statistics/edit", type: :view do
  before(:each) do
    @group_statistic = assign(:group_statistic, GroupStatistic.create!(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit group_statistic form" do
    render

    assert_select "form[action=?][method=?]", group_statistic_path(@group_statistic), "post" do

      assert_select "input#group_statistic_name[name=?]", "group_statistic[name]"

      assert_select "textarea#group_statistic_description[name=?]", "group_statistic[description]"
    end
  end
end
