require 'rails_helper'

RSpec.describe "group_statistics/show", type: :view do
  before(:each) do
    @group_statistic = assign(:group_statistic, GroupStatistic.create!(
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
