require 'rails_helper'

RSpec.describe "group_statistics/index", type: :view do
  before(:each) do
    assign(:group_statistics, [
      GroupStatistic.create!(
        :name => "Name",
        :description => "MyText"
      ),
      GroupStatistic.create!(
        :name => "Name",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of group_statistics" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
