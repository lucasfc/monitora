require 'rails_helper'

RSpec.describe "group_statistics/new", type: :view do
  before(:each) do
    assign(:group_statistic, GroupStatistic.new(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new group_statistic form" do
    render

    assert_select "form[action=?][method=?]", group_statistics_path, "post" do

      assert_select "input#group_statistic_name[name=?]", "group_statistic[name]"

      assert_select "textarea#group_statistic_description[name=?]", "group_statistic[description]"
    end
  end
end
