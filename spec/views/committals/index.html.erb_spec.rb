require 'rails_helper'

RSpec.describe "committals/index", type: :view do
  before(:each) do
    assign(:committals, [
      Committal.create!(
        :name => "Name",
        :description => "Description"
      ),
      Committal.create!(
        :name => "Name",
        :description => "Description"
      )
    ])
  end

  it "renders a list of committals" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
