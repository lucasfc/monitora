require 'rails_helper'

RSpec.describe "committals/edit", type: :view do
  before(:each) do
    @committal = assign(:committal, Committal.create!(
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit committal form" do
    render

    assert_select "form[action=?][method=?]", committal_path(@committal), "post" do

      assert_select "input#committal_name[name=?]", "committal[name]"

      assert_select "input#committal_description[name=?]", "committal[description]"
    end
  end
end
