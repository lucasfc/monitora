require 'rails_helper'

RSpec.describe "committals/new", type: :view do
  before(:each) do
    assign(:committal, Committal.new(
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new committal form" do
    render

    assert_select "form[action=?][method=?]", committals_path, "post" do

      assert_select "input#committal_name[name=?]", "committal[name]"

      assert_select "input#committal_description[name=?]", "committal[description]"
    end
  end
end
