require 'rails_helper'

RSpec.describe "committals/show", type: :view do
  before(:each) do
    @committal = assign(:committal, Committal.create!(
      :name => "Name",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
  end
end
