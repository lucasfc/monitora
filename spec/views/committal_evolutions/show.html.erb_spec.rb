require 'rails_helper'

RSpec.describe "committal_evolutions/show", type: :view do
  before(:each) do
    @committal_evolution = assign(:committal_evolution, CommittalEvolution.create!(
      :committal => nil,
      :orcamento => "9.99",
      :empenhado => "9.99",
      :liquidado => "9.99",
      :executado_financeiro => "9.99",
      :percentual_contratado => "9.99",
      :executado_vs_contratado => "9.99",
      :saldo => "9.99",
      :executado_fisico => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/9.99/)
  end
end
