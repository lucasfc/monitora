require 'rails_helper'

RSpec.describe "committal_evolutions/index", type: :view do
  before(:each) do
    assign(:committal_evolutions, [
      CommittalEvolution.create!(
        :committal => nil,
        :orcamento => "9.99",
        :empenhado => "9.99",
        :liquidado => "9.99",
        :executado_financeiro => "9.99",
        :percentual_contratado => "9.99",
        :executado_vs_contratado => "9.99",
        :saldo => "9.99",
        :executado_fisico => "9.99"
      ),
      CommittalEvolution.create!(
        :committal => nil,
        :orcamento => "9.99",
        :empenhado => "9.99",
        :liquidado => "9.99",
        :executado_financeiro => "9.99",
        :percentual_contratado => "9.99",
        :executado_vs_contratado => "9.99",
        :saldo => "9.99",
        :executado_fisico => "9.99"
      )
    ])
  end

  it "renders a list of committal_evolutions" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
