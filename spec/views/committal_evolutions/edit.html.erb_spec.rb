require 'rails_helper'

RSpec.describe "committal_evolutions/edit", type: :view do
  before(:each) do
    @committal_evolution = assign(:committal_evolution, CommittalEvolution.create!(
      :committal => nil,
      :orcamento => "9.99",
      :empenhado => "9.99",
      :liquidado => "9.99",
      :executado_financeiro => "9.99",
      :percentual_contratado => "9.99",
      :executado_vs_contratado => "9.99",
      :saldo => "9.99",
      :executado_fisico => "9.99"
    ))
  end

  it "renders the edit committal_evolution form" do
    render

    assert_select "form[action=?][method=?]", committal_evolution_path(@committal_evolution), "post" do

      assert_select "input#committal_evolution_committal_id[name=?]", "committal_evolution[committal_id]"

      assert_select "input#committal_evolution_orcamento[name=?]", "committal_evolution[orcamento]"

      assert_select "input#committal_evolution_empenhado[name=?]", "committal_evolution[empenhado]"

      assert_select "input#committal_evolution_liquidado[name=?]", "committal_evolution[liquidado]"

      assert_select "input#committal_evolution_executado_financeiro[name=?]", "committal_evolution[executado_financeiro]"

      assert_select "input#committal_evolution_percentual_contratado[name=?]", "committal_evolution[percentual_contratado]"

      assert_select "input#committal_evolution_executado_vs_contratado[name=?]", "committal_evolution[executado_vs_contratado]"

      assert_select "input#committal_evolution_saldo[name=?]", "committal_evolution[saldo]"

      assert_select "input#committal_evolution_executado_fisico[name=?]", "committal_evolution[executado_fisico]"
    end
  end
end
