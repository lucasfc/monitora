require 'rails_helper'

RSpec.describe "decisions/index", type: :view do
  before(:each) do
    assign(:decisions, [
      Decision.create!(
        :user => nil,
        :rae => nil,
        :description => "MyText",
        :status => "Status"
      ),
      Decision.create!(
        :user => nil,
        :rae => nil,
        :description => "MyText",
        :status => "Status"
      )
    ])
  end

  it "renders a list of decisions" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
