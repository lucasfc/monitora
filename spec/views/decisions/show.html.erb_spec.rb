require 'rails_helper'

RSpec.describe "decisions/show", type: :view do
  before(:each) do
    @decision = assign(:decision, Decision.create!(
      :user => nil,
      :rae => nil,
      :description => "MyText",
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Status/)
  end
end
