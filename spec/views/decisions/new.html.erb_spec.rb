require 'rails_helper'

RSpec.describe "decisions/new", type: :view do
  before(:each) do
    assign(:decision, Decision.new(
      :user => nil,
      :rae => nil,
      :description => "MyText",
      :status => "MyString"
    ))
  end

  it "renders new decision form" do
    render

    assert_select "form[action=?][method=?]", decisions_path, "post" do

      assert_select "input#decision_user_id[name=?]", "decision[user_id]"

      assert_select "input#decision_rae_id[name=?]", "decision[rae_id]"

      assert_select "textarea#decision_description[name=?]", "decision[description]"

      assert_select "input#decision_status[name=?]", "decision[status]"
    end
  end
end
