require 'rails_helper'

RSpec.describe "twitter_polls/edit", type: :view do
  before(:each) do
    @twitter_poll = assign(:twitter_poll, TwitterPoll.create!(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit twitter_poll form" do
    render

    assert_select "form[action=?][method=?]", twitter_poll_path(@twitter_poll), "post" do

      assert_select "input#twitter_poll_name[name=?]", "twitter_poll[name]"

      assert_select "textarea#twitter_poll_description[name=?]", "twitter_poll[description]"
    end
  end
end
