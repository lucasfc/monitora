require 'rails_helper'

RSpec.describe "twitter_polls/show", type: :view do
  before(:each) do
    @twitter_poll = assign(:twitter_poll, TwitterPoll.create!(
      :name => "Name",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/MyText/)
  end
end
