require 'rails_helper'

RSpec.describe "twitter_polls/index", type: :view do
  before(:each) do
    assign(:twitter_polls, [
      TwitterPoll.create!(
        :name => "Name",
        :description => "MyText"
      ),
      TwitterPoll.create!(
        :name => "Name",
        :description => "MyText"
      )
    ])
  end

  it "renders a list of twitter_polls" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
