require 'rails_helper'

RSpec.describe "twitter_polls/new", type: :view do
  before(:each) do
    assign(:twitter_poll, TwitterPoll.new(
      :name => "MyString",
      :description => "MyText"
    ))
  end

  it "renders new twitter_poll form" do
    render

    assert_select "form[action=?][method=?]", twitter_polls_path, "post" do

      assert_select "input#twitter_poll_name[name=?]", "twitter_poll[name]"

      assert_select "textarea#twitter_poll_description[name=?]", "twitter_poll[description]"
    end
  end
end
