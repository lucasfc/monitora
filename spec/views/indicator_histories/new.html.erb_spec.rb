require 'rails_helper'

RSpec.describe "indicator_histories/new", type: :view do
  before(:each) do
    assign(:indicator_history, IndicatorHistory.new(
      :indicator => nil,
      :value => "9.99",
      :status => "MyString"
    ))
  end

  it "renders new indicator_history form" do
    render

    assert_select "form[action=?][method=?]", indicator_histories_path, "post" do

      assert_select "input#indicator_history_indicator_id[name=?]", "indicator_history[indicator_id]"

      assert_select "input#indicator_history_value[name=?]", "indicator_history[value]"

      assert_select "input#indicator_history_status[name=?]", "indicator_history[status]"
    end
  end
end
