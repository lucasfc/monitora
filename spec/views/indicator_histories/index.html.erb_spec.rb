require 'rails_helper'

RSpec.describe "indicator_histories/index", type: :view do
  before(:each) do
    assign(:indicator_histories, [
      IndicatorHistory.create!(
        :indicator => nil,
        :value => "9.99",
        :status => "Status"
      ),
      IndicatorHistory.create!(
        :indicator => nil,
        :value => "9.99",
        :status => "Status"
      )
    ])
  end

  it "renders a list of indicator_histories" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
