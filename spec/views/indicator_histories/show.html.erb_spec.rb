require 'rails_helper'

RSpec.describe "indicator_histories/show", type: :view do
  before(:each) do
    @indicator_history = assign(:indicator_history, IndicatorHistory.create!(
      :indicator => nil,
      :value => "9.99",
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Status/)
  end
end
