require 'rails_helper'

RSpec.describe "twitter_counts/show", type: :view do
  before(:each) do
    @twitter_count = assign(:twitter_count, TwitterCount.create!(
      :twitter_poll => nil,
      :text => "MyText",
      :value => 1,
      :group_name => "Group Name",
      :good => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Group Name/)
    expect(rendered).to match(/false/)
  end
end
