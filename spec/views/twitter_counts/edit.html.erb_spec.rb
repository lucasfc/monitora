require 'rails_helper'

RSpec.describe "twitter_counts/edit", type: :view do
  before(:each) do
    @twitter_count = assign(:twitter_count, TwitterCount.create!(
      :twitter_poll => nil,
      :text => "MyText",
      :value => 1,
      :group_name => "MyString",
      :good => false
    ))
  end

  it "renders the edit twitter_count form" do
    render

    assert_select "form[action=?][method=?]", twitter_count_path(@twitter_count), "post" do

      assert_select "input#twitter_count_twitter_poll_id[name=?]", "twitter_count[twitter_poll_id]"

      assert_select "textarea#twitter_count_text[name=?]", "twitter_count[text]"

      assert_select "input#twitter_count_value[name=?]", "twitter_count[value]"

      assert_select "input#twitter_count_group_name[name=?]", "twitter_count[group_name]"

      assert_select "input#twitter_count_good[name=?]", "twitter_count[good]"
    end
  end
end
