require 'rails_helper'

RSpec.describe "twitter_counts/new", type: :view do
  before(:each) do
    assign(:twitter_count, TwitterCount.new(
      :twitter_poll => nil,
      :text => "MyText",
      :value => 1,
      :group_name => "MyString",
      :good => false
    ))
  end

  it "renders new twitter_count form" do
    render

    assert_select "form[action=?][method=?]", twitter_counts_path, "post" do

      assert_select "input#twitter_count_twitter_poll_id[name=?]", "twitter_count[twitter_poll_id]"

      assert_select "textarea#twitter_count_text[name=?]", "twitter_count[text]"

      assert_select "input#twitter_count_value[name=?]", "twitter_count[value]"

      assert_select "input#twitter_count_group_name[name=?]", "twitter_count[group_name]"

      assert_select "input#twitter_count_good[name=?]", "twitter_count[good]"
    end
  end
end
