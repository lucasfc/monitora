require 'rails_helper'

RSpec.describe "twitter_counts/index", type: :view do
  before(:each) do
    assign(:twitter_counts, [
      TwitterCount.create!(
        :twitter_poll => nil,
        :text => "MyText",
        :value => 1,
        :group_name => "Group Name",
        :good => false
      ),
      TwitterCount.create!(
        :twitter_poll => nil,
        :text => "MyText",
        :value => 1,
        :group_name => "Group Name",
        :good => false
      )
    ])
  end

  it "renders a list of twitter_counts" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Group Name".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
