require 'rails_helper'

RSpec.describe "import_files/new", type: :view do
  before(:each) do
    assign(:import_file, ImportFile.new(
      :file => "MyText",
      :category => "MyString"
    ))
  end

  it "renders new import_file form" do
    render

    assert_select "form[action=?][method=?]", import_files_path, "post" do

      assert_select "textarea#import_file_file[name=?]", "import_file[file]"

      assert_select "input#import_file_category[name=?]", "import_file[category]"
    end
  end
end
