require 'rails_helper'

RSpec.describe "import_files/edit", type: :view do
  before(:each) do
    @import_file = assign(:import_file, ImportFile.create!(
      :file => "MyText",
      :category => "MyString"
    ))
  end

  it "renders the edit import_file form" do
    render

    assert_select "form[action=?][method=?]", import_file_path(@import_file), "post" do

      assert_select "textarea#import_file_file[name=?]", "import_file[file]"

      assert_select "input#import_file_category[name=?]", "import_file[category]"
    end
  end
end
