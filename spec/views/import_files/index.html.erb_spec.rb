require 'rails_helper'

RSpec.describe "import_files/index", type: :view do
  before(:each) do
    assign(:import_files, [
      ImportFile.create!(
        :file => "MyText",
        :category => "Category"
      ),
      ImportFile.create!(
        :file => "MyText",
        :category => "Category"
      )
    ])
  end

  it "renders a list of import_files" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
  end
end
