require 'rails_helper'

RSpec.describe "import_files/show", type: :view do
  before(:each) do
    @import_file = assign(:import_file, ImportFile.create!(
      :file => "MyText",
      :category => "Category"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Category/)
  end
end
