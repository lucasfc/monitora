require 'rails_helper'

RSpec.describe PowerbiController, type: :controller do

  describe "GET #orcamento" do
    it "returns http success" do
      get :orcamento
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #receita" do
    it "returns http success" do
      get :receita
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #evolucao" do
    it "returns http success" do
      get :evolucao
      expect(response).to have_http_status(:success)
    end
  end

end
