require 'rails_helper'

RSpec.describe SaudeController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #medicamentos" do
    it "returns http success" do
      get :medicamentos
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #estoque" do
    it "returns http success" do
      get :estoque
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #cirurgias_eletivas" do
    it "returns http success" do
      get :cirurgias_eletivas
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #ocupacao" do
    it "returns http success" do
      get :ocupacao
      expect(response).to have_http_status(:success)
    end
  end

end
