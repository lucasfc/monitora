require 'rails_helper'

RSpec.describe PublicoController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #orcamento" do
    it "returns http success" do
      get :orcamento
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #entregas" do
    it "returns http success" do
      get :entregas
      expect(response).to have_http_status(:success)
    end
  end

end
