require 'rails_helper'

RSpec.describe EconomiaController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #subitem" do
    it "returns http success" do
      get :subitem
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #sgd" do
    it "returns http success" do
      get :sgd
      expect(response).to have_http_status(:success)
    end
  end

end
