require 'rails_helper'

RSpec.describe ReceitaController, type: :controller do

  describe "GET #avaliacao" do
    it "returns http success" do
      get :avaliacao
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #relatorio" do
    it "returns http success" do
      get :relatorio
      expect(response).to have_http_status(:success)
    end
  end

end
