module ApplicationHelper
	def div_desempenho(nome, valor, reverse=false)
		if valor.blank?
			valor = 0
		end
		if reverse
			if valor == 0
				color= "blue"
				icon= "twa twa-smiley"
			elsif valor < 30
				color= "green"
				icon= "twa twa-smiley"
			elsif valor < 50
				color= "amber darken-3"
				icon= "twa twa-neutral-face"
			else
				color= "red"
				icon= "twa twa-worried"
			end
		else
			if valor < 50
				color= "red"
				icon= "twa twa-worried"
			elsif valor < 70
				color= "amber darken-3"
				icon= "twa twa-neutral-face"
			elsif valor == 100
				color= "blue"
				icon= "twa twa-smiley"
			else
				color= "green"
				icon= "twa twa-smiley"
			end
		end
		{

		html: "
		<div class='card col s12 #{color}'>
				<table style='width:100%'>
					<tr>
					<td colspan='2' class='center white-text' style='padding:0px;'>
						#{nome}
					</td>
					</tr>
					<tr>
					<td class='center'  style='padding:0px;'><i class=' fa-2x #{icon}'></i></td>
					<td class='white-text '  style='padding:0px;'><strong>#{valor.round(1)}%</strong></td>
					</tr>
				</table>
		</div>",
		color: color,
		icon: icon,
		value: valor
		}
	end

	def div_desempenho2(nome, valor, reverse=false)
		if valor.blank?
			valor = 0
		end
	"
		<div class='card col s12 teal'>
				<table style='width:100%'>
					<tr>
					<td class='center white-text' style='padding:0px;'>
						#{nome}
					</td>
					</tr>
					<tr>
					<td class='center white-text' style='padding:0px;'>
						<strong>#{valor.round(1)}%</strong>
					</td>
					</tr>
				</table>
		</div>
	"
	end

	def div_desempenho3(nome, valor, reverse=false)
		if valor.blank?
			valor = 0
		end
		if reverse
			if valor == 0
				color= "blue"
				icon= "twa twa-smiley"
				social_fala = "As pessoas estão falando muito bem nas redes sociais do tema #{nome}."
			elsif valor < 30
				color= "green"
				icon= "twa twa-smiley"
				social_fala = "As pessoas estão falando muito bem nas redes sociais do tema #{nome}."
			elsif valor < 50
				color= "amber darken-3"
				icon= "twa twa-neutral-face"
				social_fala = "As pessoas estão um pouco divididas, alguns falam bem, outros mal sobre o tema #{nome}"
			else
				color= "red"
				icon= "twa twa-worried"
				social_fala = "A coisa não está boa, as pessoas não estão falando bem, sobre o tema #{nome}"
			end
		else
			if valor < 30
				color= "red"
				icon= "twa twa-worried"
				social_fala = "A coisa não está boa, as pessoas não estão falando bem, sobre o tema #{nome}"
			elsif valor < 50
				color= "amber darken-3"
				icon= "twa twa-neutral-face"
				social_fala = "As pessoas estão um pouco divididas, alguns falam bem, outros mal sobre o tema #{nome}"
			elsif valor == 100
				color= "blue"
				icon= "twa twa-smiley"
				social_fala = "As pessoas estão falando muito bem nas redes sociais do tema #{nome}."
			else
				color= "green"
				icon= "twa twa-smiley"
				social_fala = "As pessoas estão falando muito bem nas redes sociais do tema #{nome}."
			end
		end
		{

		html: "
		<div class='card col s12 #{color}'>
				<table style='width:100%'>
					<tr>
					<td colspan='2' class='center white-text' style='padding:0px;'>
						#{nome}
					</td>
					</tr>
					<tr>
					<td class='center'  style='padding:0px;'><i class=' fa-2x #{icon}'></i></td>
					<td class='white-text '  style='padding:0px;'><strong>#{valor.round(1)}%</strong></td>
					</tr>
				</table>
		</div>",
		color: color,
		icon: icon,
		value: valor,
		social_fala: social_fala
		}
	end
	def color_finance(value)
		if value.blank? or value > -1
			"green"
		else
			"red"
		end
	end

	def days_to_now_s(date)
		(Date.strptime(date, '%d/%m/%y') - DateTime.now.to_date).to_i
	end

	def calc_percent(total, percent)
		begin
			result = ((percent.to_i * 100) / total.to_i) - 100
		rescue
			result = "**"
		end
		"#{result} %"
	end

	def calc_percent_total(total, percent)
		begin
			result = ((percent.to_i * 100) / total.to_i)
		rescue
			result = "**"
		end
		"#{result} %"
	end

	def ranking_color(value)
		if value.to_i <= -20
			"green-text"
		else
			"red-text"
		end
	end
end
