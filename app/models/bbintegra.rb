class Bbintegra < ActiveRecord::Base
	def self.url
		'http://bbintegra.com.br'
	end

	def self.call(method , endereco, params)
		if method == :get
			params_url  = URI.unescape(params.to_param)
			endereco = url + endereco +"?"+ params_url
			call = RestClient.send(method, endereco)
		else
			endereco = url + endereco
			call = RestClient.send(method, endereco, params)
		end
		page = Nokogiri::HTML(call)
		page.css("div#box")
	end
end
