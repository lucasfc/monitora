class QuestionReply < ActiveRecord::Base
	has_paper_trail
	belongs_to :question
	belongs_to :reply
end
