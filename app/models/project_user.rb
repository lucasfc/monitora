class ProjectUser < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  has_paper_trail

  extend Enumerize
  enumerize :role, in: [:alimentar, :editar], predicates: true, default: :alimentar
end
