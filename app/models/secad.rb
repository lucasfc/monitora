class Secad < ActiveRecord::Base
  self.table_name = 'serv_servidor'
  establish_connection "secad".to_sym

  default_scope -> {where(situacao: "ATIVO")}

  def to_s
    self.nome
  end

  def self.calc_folha
    calc = 0
    where("salario <> 0 AND  gratificacao <> 0").map do |servidor|
      if (servidor.gratificacao * 0.6) < servidor.salario
      calc += ((servidor.gratificacao * 0.4) + servidor.salario).round(2)
      else
        calc += servidor.gratificacao
      end
    end

    calc += where("salario = 0 AND  gratificacao <> 0").sum(:gratificacao)
    calc += where("salario <> 0 AND  gratificacao = 0").sum(:salario)

  end
end
