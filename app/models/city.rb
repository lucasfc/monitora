class City < ActiveRecord::Base
  has_many :city_links

  accepts_nested_attributes_for :city_links, :allow_destroy => true

  def set_links
    if city_links.blank?
      CityLink.category.options.map do |category|
        city_links.build(category: category[1])
      end
    end
  end

end
