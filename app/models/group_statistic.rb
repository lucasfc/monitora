class GroupStatistic < ActiveRecord::Base
	has_many :statistics
	has_paper_trail

	default_scope -> {where(remove: [nil, false])}
end
