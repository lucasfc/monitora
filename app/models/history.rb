class History < ActiveRecord::Base
  has_paper_trail
  belongs_to :step
  belongs_to :user
  has_many :history_files

  accepts_nested_attributes_for :history_files, :allow_destroy => true

  after_save :set_status_step

  default_scope -> {order(created_at: :desc)}

  def set_status_step
  	step.status = step.get_status
  	step.save
  end

  def photos
  	history_files.where(type: :photo)
  end
  
  def videos
  	history_files.where(type: :video)
  end

  def links
    URI.extract(description, ["http","https"])
  end
end
