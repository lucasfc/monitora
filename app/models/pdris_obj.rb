class PdrisObj < ActiveRecord::Base
	self.table_name = 'vw_seplan_painel_situacao_objeto'
	establish_connection "pdris".to_sym
	self.primary_key = "id_objeto"

	has_many :pdris_histories, foreign_key: :id_objeto

	def self.executado_vs_contratado
		if total_contratado == 0
			0
		else			
			((total_medido*100)/total_contratado).round(1)
		end
	end

	def self.executado
		((total_medido*100)/total_credito).round(1)
	end

	def self.total_contratado
		begin
			PdrisObj.all.sum(:vlr_total_contratado_dolar).round(2)
		rescue
		0
		end	
	end

	def self.contratado
		((total_contratado*100)/total_credito).round(1)
	end

	def self.total_medido
		begin
		PdrisObj.all.sum(:vlr_total_medido_dolar).round(2)
		rescue
		0
		end
	end

	def self.total_credito
		375000000.00
	end

	def self.saldo
		(total_credito - total_medido).round(2)
	end

	def executado
		if vlr_total_contratado_dolar == 0
			0
		else
			((self.vlr_total_medido_dolar * 100)/ self.vlr_total_contratado_dolar).round(2)
		end
	end

	def self.service_status
		begin
			PdrisObj.all.sum(:vlr_total_contratado_dolar).round(2)
			return true
		rescue
			false
		end	
		
	end

end
