class DomainCheck 
	def initialize(domain)
		@domains = [domain].flatten
	end

	def matches?(request)
		@domains.include? request.host
	end
end
