class ImportFile < ActiveRecord::Base
	has_paper_trail
	mount_uploader :file, DocumentUploader

	after_save :update_file_json

	extend Enumerize
	enumerize :category, in: [:emenda, :convenios, :emenda_2016], predicates: true

	def update_file_json
		if emenda?
			Emendas.get_file(self.file)
			Emendas.set_index_json
		end
		if emenda_2016?
			Emendas.get_file(self.file,2016)
			Emendas.set_index_2016
		end
		if convenios?
			Convenios.get_file(self.file)
			Convenios.set_index_json
		end
	end
	
end
