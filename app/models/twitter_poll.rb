class TwitterPoll < ActiveRecord::Base
	has_paper_trail
	has_many :twitter_counts
	
	accepts_nested_attributes_for :twitter_counts, :allow_destroy => true

	def good_percent
		((twitter_counts.where(good: true).sum(:value) *100) / twitter_counts.sum(:value)).round(2)
	end
end
