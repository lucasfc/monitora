class Rae < ActiveRecord::Base
	has_paper_trail
	belongs_to :user
	has_many :decisions
	has_many :projects, through: :decisions

	mount_uploader :ata, PdfUploader

	accepts_nested_attributes_for :decisions, :allow_destroy => true
end
