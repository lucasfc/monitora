class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)
    if user.admin?
        can :manage, :all
    end

    #Gestores
    if user.gestor or user.gestor_seplan?
        can :manage, :home
        can :read, GroupStatistic
        can :read, TwitterPoll
        can :read, Statistic
        can :manage, ImportFile
        can :manage, :emendas
    end
    #Seplan
    if user.seplan?
        can :manage, Decision
        can :manage, Rae
        can :manage, QuestionReply
        can :manage, Reply
        can :manage, IndicatorHistory
        can :manage, Indicator
        can :manage, History
        can :manage, Project
        can :manage, GroupStatistic
        can :manage, TwitterPoll
        can :manage, Statistic
        can :manage, City
        cannot :read, :emendas
    end

    #Estatística
    if user.stats?
        can :manage, GroupStatistic
        can :manage, TwitterPoll
        can :manage, Statistic
    end

    #Técnicos
    if user.comum?
        can :create, Project
        can :create, History
        can :create, Reply
        can :create, IndicatorHistory
        can :goals, Indicator
        can :deed_acompanhamento, Indicator
        can :deed_evaluation, Indicator
        can :histories, Indicator
        can :analyze, Indicator
        can :create, YearHistory
        can :update, YearHistory do |history|
            history.user == user
        end
        can :read, YearHistory
    end

    #Home
        can :gestores, :home
        can :receita, :home
        can :orcamento, :home
        can :despesa, :home
        can :tocantins_em_numeros, :home
        can :cidades, :home

    #Read
        can :read, :all

    #Project
        can :orcamento, Project
        can :my, Project
        can :update, Project do |project|
            project.user_can_edit(user)
        end

    #Indicator
        can :projects, Indicator
        can :entregas_combo, Indicator
        can :probabilidade, Indicator
        can :year_history, Indicator

    #History
        can :update, History do |history|
            user == history.step.project.user
        end

    #Reply
        can :update, Reply do |reply|
            user == reply.user
        end

    #Committals
        can :projects, Committal

    #Convenios
        can :manage, :convenios

    #Cidades
        can :read, City

    #Cannot

        if !user.gestor and  !user.gestor_seplan? and  !user.admin?
            cannot :read, :emendas
            cannot :read, GroupStatistic
        end

        if !user.gestor and  !user.admin?        
            cannot :read, TwitterCount
            cannot :read, TwitterPoll
            cannot :read, Statistic
            cannot :redes_sociais, :home
        end

    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
