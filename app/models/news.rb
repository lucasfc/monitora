class News < ActiveRecord::Base
	belongs_to :user
	extend Enumerize
  enumerize :status, in: [:enviado, :descartado, :aprovado], predicates: true, default: :enviado
end
