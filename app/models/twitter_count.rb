class TwitterCount < ActiveRecord::Base
	belongs_to :twitter_poll
	before_validation :set_value

	def get_twitter_count
		since_id = self.last_tweet ||  ""
		tweets = Social.client.search(self.text, result_type: "recent", since_id:since_id)
		if tweets.first.present?
			last_value = self.value || 0			
			self.value = last_value += tweets.count
			self.last_tweet = tweets.first.id
		end
		save
		self.value
	end

	def self.refresh_tweets
		all.map { |e| puts e.get_twitter_count  }
	end

	def set_value
		self.value = self.value || 0
	end
end
