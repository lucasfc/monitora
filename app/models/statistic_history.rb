class StatisticHistory < ActiveRecord::Base
  belongs_to :statistic
  has_paper_trail

  extend Enumerize
  enumerize :caret, in: [:bom, :ruim, :indiferente], predicates: true, default: :indiferente
end
