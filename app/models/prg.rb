class Prg < ActiveRecord::Base
	def self.url
		'http://prg.seplan.to.gov.br'
	end

	def self.call(method , endereco, params)
		if method == :get
			params_url  = URI.unescape(params.to_param)
			endereco = url + endereco +"?"+ params_url
			call = RestClient.send(method, endereco)
		else
			endereco = url + endereco
			call = RestClient.send(method, endereco, params)
		end
		return JSON.parse(call, symbolize_names: true)
	end
end
