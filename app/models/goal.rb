class Goal < ActiveRecord::Base
  belongs_to :indicator
  has_paper_trail
end
