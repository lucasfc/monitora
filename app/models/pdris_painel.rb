class PdrisPainel < ActiveRecord::Base
	self.table_name = 'vw_seplan_painel_item'
	establish_connection "pdris".to_sym
	#self.primary_key = "id_objeto"

	def self.contratados
		where.not(quant_total_contratada: nil)
	end
end
