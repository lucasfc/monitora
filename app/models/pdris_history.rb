class PdrisHistory < ActiveRecord::Base
	self.table_name = 'vw_seplan_painel_objeto_item'
	establish_connection "pdris".to_sym
	self.primary_key = "id_item"

	belongs_to :pdris_obj, foreign_key: :id_objeto

	def executado
		if vlr_total_contratado_dolar == 0
			0
		elsif vlr_total_contratado_dolar < vlr_total_medido_dolar
			100.0
		else
			((self.vlr_total_medido_dolar * 100)/ self.vlr_total_contratado_dolar).round(2)
		end
	end
end
