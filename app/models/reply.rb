class Reply < ActiveRecord::Base
	has_paper_trail
	belongs_to :indicator
	belongs_to :user
	has_many :question_replies, dependent: :destroy
	has_many :reply_committals

	accepts_nested_attributes_for :question_replies
	accepts_nested_attributes_for :reply_committals, :allow_destroy => true
end
