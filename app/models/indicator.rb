class Indicator < ActiveRecord::Base
	has_paper_trail
	has_many :goals
	has_many :projects
	has_many :indicator_histories
	has_many :replies
	has_many :committals
	has_many :year_histories
	belongs_to :user
	belongs_to :gestor, :class_name => "User"
	belongs_to :twitter_poll

	accepts_nested_attributes_for :goals, :allow_destroy => true
	accepts_nested_attributes_for :committals, :allow_destroy => true

	default_scope -> {order(:ord)}
	scope :externals, -> {where(external: true)}
	extend Enumerize
	self.inheritance_column = nil
	enumerize :type, in: [:crescente, :decrescente], predicates: true
	
	def to_s
		self.name
	end

	def last_data
		if indicator_histories
			indicator_histories.order(date: :asc).last
		end
	end

	def final_goal
		if goals.present?
			goals.order(date: :asc).last
		end
	end

	def next_goals
		goals.where('date > ?', DateTime.now)
	end

	def next_goal
		if next_goals.present?
			next_goals.first
		else
			{}
		end
	end

	def media_crescimento
		begin
			historico = indicator_histories.order(date: :desc).limit(10).reverse
			la = 0
			sum_percents = 0
			percentuais_diff = historico.map do |h|
				if la != 0
					a = (h.value*100)/la					
					sum_percents += a			
				end
					la = h.value
			end
			l = historico.count() - 1
			((sum_percents / l ) - 100).to_i
		rescue
			nil
		end
	end

	#Calcula se a entrega será alcançada
	def meta_sera_alcancada?
		begin
			meta = next_goal
			apurado  = last_data
			if apurado.value >= meta.value
				return true
			end
			indicator_histories_year = indicator_histories.order(date: :desc).limit(10).reverse
			dias = (indicator_histories_year.last.date - indicator_histories_year.first.date).to_i
			dias_restantes = (meta.date.to_date - DateTime.now.to_date).to_i
			evolucao = (indicator_histories_year.last.value - indicator_histories_year.first.value)
			valor_no_dia = ((evolucao * dias_restantes) / dias) + apurado.value
			valor_no_dia >= meta.value
		rescue
			nil
		end
	end

	def probabilidade
		p1 = (committals.sum(:probabilidade)/committals.count)
		p2 = meta_sera_alcancada? ? 200 : 60
		(p1 + p2)/3
	end

	def orcamento
		committals.sum(:orcamento)
	end

	def liquidado
		committals.sum(:liquidado)
	end

	def empenhado
		committals.sum(:empenhado)
	end

	def alteracoes
		committals.sum(:changes_amount)
	end

	def nao_executado
		orcamento - liquidado
	end

	def sumary
		begin
			reply = replies.order(finish_date: :desc).first
			reply.sumary_seplan || reply.sumary
		rescue
			"Não Realizada"
		end
	end

	def suggestion
		begin
			reply = replies.order(finish_date: :desc).first
			reply.suggestion_seplan || reply.suggestion
		rescue
			"Não Realizada"
		end
	end

	def gestor_name
		begin
			gestor.name
		rescue
			"Não Informado"
		end
	end

	def planeja_deeds(user)
		JSON.parse(
			RestClient::Request.execute(
				url:"http://planejamento.monitora.to.gov.br/deeds.json?q[monitora_indicator_eq]=#{self.id}&access_token=#{user.token}",
				 verify_ssl: false,
				 method: :get
			), symbolize_names: true)
	end

end
