class YearHistory < ActiveRecord::Base
  belongs_to :indicator
  belongs_to :user

  before_save :set_date

  def set_date
    self.date = self.date.beginning_of_month
  end
end
