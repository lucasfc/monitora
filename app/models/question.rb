class Question < ActiveRecord::Base
	has_paper_trail
	scope :actives, -> { where(active: true)}
end
