class User < ActiveRecord::Base
  has_paper_trail
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  has_many :projects
  has_many :histories
  has_many :replies
  has_many :indicators
  has_many :indicator_histories
  has_many :indicators_as_gestor, class_name: "Indicator", foreign_key: "gestor_id"
  
  extend Enumerize
  enumerize :role, in: [:seplan, :admin, :comum, :stats, :view, :gestor_seplan], predicates: true, default: :view

  #before_validation :add_to_seplan, on: :create

  def toauth_fast
	 ToauthConnect.fastdata(self)
  end

  def toauth_data
	 ToauthConnect.profile_data(self)
  end

  def name_short
    self.name.split(' ')[0]
  end

  def name_medium
    "#{self.name.split(' ').first} #{self.name.split(' ')[1]} #{self.name.split(' ').last} "
  end

  def add_to_seplan
    begin
      if toauth_fast[:secad][:cod_orgao] == 21
        self.role = :seplan
      end
    rescue
    end
  end
end
