class Convenios < ActiveRecord::Base
	require 'csv'
	def self.get_file(file)
		csv_text = File.open("#{Rails.root}/public#{file}", "r:ISO-8859-1")
		csv = CSV.parse(csv_text, :headers => false, :col_sep => ";")
		json = csv.to_json.force_encoding("utf-8")
		File.open("public/planilhas/convenios.json","w") do |f|
		  f.write(json)
		end
	end

	def self.get_json
		JSON.load(open("#{Rails.root}/public/planilhas/convenios.json"))
	end

	def self.get_resume
		JSON.load(open("#{Rails.root}/public/planilhas/convenios_resume.json"))
	end

	def self.set_index_json
		lista = get_json.group_by{ |d| d[0] }.map do |orgao, itens|
			if orgao.present?				
				uniao = itens.sum{|a| a[8] ? a[8].tr(',', '').to_f : 0} || 0
				contrapartida = itens.sum{|a| a[9] ? a[9].tr(',', '').to_f : 0} || 0
				pagamentos = itens.sum{|a| a[12] ? a[12].tr(',', '').to_f : 0} || 0
				total_div = (contrapartida+uniao)
				{
					name: orgao,
					quantidade: itens.count,
					uniao: uniao,
					desembolso: itens.sum{|a| a[10] ? a[10].tr(',', '').to_f : 0},
					contrapartida: contrapartida,
					ingresso: itens.sum{|a| a[11] ? a[11].tr(',', '').to_f : 0},
					pagamentos: pagamentos,
					execucao: total_div == 0 ? 0 : ((pagamentos/(total_div))*100).round(2)
				}
			end
		end.compact
		File.open("public/planilhas/convenios_resume.json","w") do |f|
		  f.write(lista.to_json)
		end
	end
end
