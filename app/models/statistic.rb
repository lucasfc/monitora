class Statistic < ActiveRecord::Base
  belongs_to :group_statistcs
  has_many :statistic_histories
  has_paper_trail

  accepts_nested_attributes_for :statistic_histories, :allow_destroy => true

  def last_data
		if statistic_histories
			statistic_histories.order(date: :asc).last
		end
	end
end
