class Step < ActiveRecord::Base
  has_paper_trail
  belongs_to :project
  has_many :histories

  extend Enumerize
  self.inheritance_column = nil
  enumerize :status, in: [:nao_iniciada, :em_andamento, :finalizada, :atrasada], predicates: true, default: :nao_iniciada

  validate :range_date

  before_validation :set_days

  def to_s
    self.name
  end

  def range_date
    if self.estimated_date < self.start_date
      errors.add("Data Final", "não pode ser menor que data inicial")
    end
  end

  def set_days
  	self.days = (self.estimated_date - self.start_date).to_i
  end

  def get_status
  	if histories.where(finish: true).present?
  		:finalizada
  	elsif start_date > DateTime.now and histories.blank?
  		:nao_iniciada
  	elsif start_date < DateTime.now and histories.blank?
  		:atrasada
  	else
  		:em_andamento
  	end
  end

  def self.set_atrasadas
  	Step.where("estimated_date < ? AND status <> ?", DateTime.now, "finalizada").update_all(status: :atrasada)
  	Step.where("start_date < ? AND status = ?", DateTime.now, "nao_iniciada").update_all(status: :atrasada)
  end

end
