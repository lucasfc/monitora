class Emendas < ActiveRecord::Base
	require 'csv'
	def self.get_file(file, year=nil)
		csv_text = File.open("#{Rails.root}/public#{file}", "r:ISO-8859-1")
		csv = CSV.parse(csv_text, :headers => false, :col_sep => ";")
		json = csv.to_json.force_encoding("utf-8")
		File.open("public/planilhas/emendas#{year}.json","w") do |f|
		  f.write(json)
		end
	end

	def self.get_json(year=nil)
		JSON.load(open("#{Rails.root}/public/planilhas/emendas#{year}.json"))
	end

	def self.get_deputados
		JSON.load(open("#{Rails.root}/public/deputados.json"))
	end

	def self.get_resume(year=nil)
		JSON.load(open("#{Rails.root}/public/planilhas/emendas_resume#{year}.json"))
	end


	def self.set_index_json
		itens = get_json
		lista = get_deputados.map do |deputado|
			itens_by_dpt = itens.select {|a| a[1] == deputado['source'].to_s }
			#binding.pry
			pago_2015 = itens_by_dpt.sum{|a| a[12].tr(',', '').to_d rescue 0} || 0
			pago_2016 = itens_by_dpt.sum{|a| a[13].tr(',', '').to_d rescue 0} || 0
			{
				name: deputado['name'],
				source: deputado['source'],
				img: deputado['img'],
				empenhado: itens_by_dpt.sum{|a| a[9].tr(',', '').to_d rescue 0},
				liquidado: itens_by_dpt.sum{|a| a[11].tr(',', '').to_d rescue 0},
				pago_2015: pago_2015,
				pago_2016: pago_2016,
				total: pago_2016 + pago_2015
			}
		end
		File.open("public/planilhas/emendas_resume.json","w") do |f|
		  f.write(lista.to_json)
		end
	end

	def self.set_index_2016
		itens = get_json(2016)
		lista = get_deputados.map do |deputado|
			itens_by_dpt = itens.select {|a| a[8] == deputado['source'].to_s }
			
			total_emendas = 2990000
			solicitado = itens_by_dpt.sum{|a| a[14].tr(',', '').to_d rescue 0} || 0
			{
				name: deputado['name'],
				source: deputado['source'],
				img: deputado['img'],
				empenhado: itens_by_dpt.sum{|a| a[15].tr(',', '').to_d rescue 0},
				liquidado: itens_by_dpt.sum{|a| a[16].tr(',', '').to_d rescue 0},
				pago: itens_by_dpt.sum{|a| a[17].tr(',', '').to_d rescue 0} || 0,
				solicitado: solicitado,
				total: total_emendas,
				a_solicitar: total_emendas - solicitado
			}
		end
		File.open("public/planilhas/emendas_resume2016.json","w") do |f|
		  f.write(lista.to_json)
		end
	end
end
