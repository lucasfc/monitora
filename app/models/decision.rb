class Decision < ActiveRecord::Base
	has_paper_trail
	belongs_to :user
	belongs_to :rae
	has_many :projects, dependent: :destroy

	extend Enumerize
	enumerize :status, in: [:sem_monitoramento, :em_andamento, :cumprida], predicates: true, default: :sem_monitoramento
end
