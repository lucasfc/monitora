class Committal < ActiveRecord::Base
	belongs_to :indicator
	has_many :projects
	has_many :steps, through: :projects
	has_many :histories, through: :steps
	has_many :history_files, through: :histories 
	has_many :committal_evolutions, dependent: :destroy
	has_many :histories_projects, through: :projects
	has_many :reply_committals
	has_paper_trail

	def to_s
		self.name
	end

	def photos
		history_files.where(type: :photo).order(created_at: :desc)
	end

	def find_photo
		if photos.present?			
			photos.first.file.url
		else
			begin
				histories_projects.order(id: :desc)
				.select{|a| a.photos.present?}
				.first.photos.first.file.url
			rescue
				"/bg-tocantins.jpg"
			end
		end
	end

	def calc_start_date
		begin
			steps.where.not(start_date: nil).sort{|a, b| a.start_date <=> b.start_date }.first.start_date
		rescue
		end
	end

	def calc_estimate_date
		begin
			steps.where.not(estimated_date: nil).sort{|a, b| b.estimated_date <=> a.estimated_date }.first.estimated_date
		rescue
		end
	end

	def calc_orcamento
		projects.sum(:budget)
	end

	def calc_alteracoes
		projects.sum(:changes_amount)
	end

	def calc_empenhado
		projects.sum(:committed)
	end

	def calc_liquidado
		projects.sum(:paid)
	end

	def calc_executado_financeiro
		if calc_orcamento == 0
			0.0
		else			
			((calc_liquidado*100)/calc_orcamento).round(2)
		end
	end


	def calc_percentual_contratado
		if calc_orcamento == 0
			0.0
		else			
			((calc_empenhado*100)/calc_orcamento).round(2)
		end
	end

	def calc_executado_vs_contratado
		if calc_empenhado == 0
			0.0
		else			
			((calc_liquidado*100)/calc_empenhado).round(2)
		end
	end

	def calc_saldo
		calc_orcamento - calc_liquidado
	end

	def calc_executado_fisico
		begin 
			days = steps.sum(:days)
			fixed = steps.where(status: :finalizada).sum(:days)
			((fixed*100)/days).round(2)
		rescue
			0
		end
	end

	def set_calcs
		update(
			photo: find_photo,
			start_date: calc_start_date,
			estimate_date: calc_estimate_date,
			orcamento: calc_orcamento,
			empenhado: calc_empenhado,
			liquidado: calc_liquidado,
			changes_amount: calc_alteracoes,
			executado_financeiro: calc_executado_financeiro,
			percentual_contratado: calc_percentual_contratado,
			executado_vs_contratado: calc_executado_vs_contratado,
			saldo: calc_saldo,
			executado_fisico: calc_executado_fisico,
			probabilidade: probalidade_de_conclusao
			)
	end

	def self.run_calculations
		Committal.all.map { |c| c.set_calcs  }
	end

	def save_history
		CommittalEvolution.create(
			committal: self,
			orcamento: self.orcamento,
			empenhado: self.empenhado,
			liquidado: self.liquidado,
			executado_financeiro: self.executado_financeiro,
			percentual_contratado: self.percentual_contratado,
			executado_vs_contratado: self.executado_vs_contratado,
			saldo: self.saldo,
			executado_fisico: self.executado_fisico,

		)
	end


	def self.save_histories
		Rake::Task['update_projects:update_steps'].invoke
		Rake::Task['update_projects:update_indices'].invoke
		puts "--Excluíndo Histórico do dia"
		CommittalEvolution.select{|a| a.created_at.today?}.map { |e| e.delete  }
		Committal.all.map { |c| c.save_history  }
	end

	def calc_prop_tarefas
		begin
			#calcula a probalidade de atingir as tarefas
			qdd_steps =  steps.count
			steps_delay = steps.where.not(status: :atrasada)

			p1 = (steps_delay.count*100)/qdd_steps
			p2 = (steps_delay.sum(:percent) / steps_delay.count)
			r = ((p1+ p2)/2).to_i
			if r > 98
				return 98
			elsif r < 0
				return 0			
			end
			r
		rescue
			0
		end
	end

	def calc_prop_execucao
		begin
		#calcula a probalidade de execucao financeira
		qdd_dias = Date.today.yday
		executado = executado_financeiro
		executado_por_dia = (executado/qdd_dias)
		meta_dia = 0.273972603
		r = ((executado_por_dia*100)/meta_dia).to_i

		if r > 98
			return 98
		elsif r < 0
			return 0			
		end
		r
		rescue
			0
		end
	end

	def calc_prop_contratacao
		begin
		#calcula a probalidade de execucao financeira
		qdd_dias = Date.today.yday
		contratado = percentual_contratado
		contratado_por_dia = (contratado/qdd_dias)
		meta_dia = 0.273972603
		r = ((contratado_por_dia*100)/meta_dia)
		if r > 98
			return 98
		elsif r < 0
			return 0			
		end
		r
		rescue
			0
		end
	end

	def probalidade_de_conclusao
		((calc_prop_tarefas + calc_prop_execucao + calc_prop_contratacao)/3).to_i
	end
end
