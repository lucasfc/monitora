class IndicatorHistory < ActiveRecord::Base
	has_paper_trail
	belongs_to :indicator

	def month
		"#{self.date.month}/#{self.date.year}"
	end
end
