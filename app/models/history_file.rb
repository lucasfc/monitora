class HistoryFile < ActiveRecord::Base
	has_paper_trail
	belongs_to :history
	mount_uploader :file, DocumentUploader
	extend Enumerize
	self.inheritance_column = nil
	enumerize :type, in: [:photo, :video, :pdf], predicates: true
end
