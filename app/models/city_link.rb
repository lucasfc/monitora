class CityLink < ActiveRecord::Base
  belongs_to :city

  extend Enumerize
  enumerize :category, in: [:seplan, :tce, :ibge], predicates: true
end
