class Project < ActiveRecord::Base
  has_paper_trail
  belongs_to :user
  belongs_to :project
  belongs_to :indicator
  belongs_to :decision
  belongs_to :committal
  has_many :steps
  has_many :histories, through: :steps
  has_many :history_files, through: :histories
  has_many :projects
  has_many :histories_projects, through: :projects, source: :histories
  has_many :project_deeds
  has_many :project_users
  has_many :users, through: :project_users

  default_scope {order(:name)}

  accepts_nested_attributes_for :steps, :allow_destroy => true
  accepts_nested_attributes_for :project_deeds, :allow_destroy => true
  accepts_nested_attributes_for :project_users, :allow_destroy => true

  extend Enumerize
  enumerize :status, in: [:nao_iniciado, :em_andamento, :finalizado, :atrasado], predicates: true, default: :nao_iniciado

  before_validation :set_default_indices, on: :create
  before_validation :get_budget_info

  def all_histories
    histories + histories_projects
  end

  def image
    begin
      history_files.where(type: :photo).last.file.url
    rescue
      "/bg-tocantins.jpg"
    end
  end

  def update_estimates
    self.estimated_date = steps.order(estimated_date: :desc).first.estimated_date
    self.start_date = steps.order(start_date: :desc).first.start_date
    days = steps.sum(:days)
    steps.map do |step|
      step.percent = (step.days * 100)/days
      step.save
    end     
    save
  end


  def calc_indice_atualizacao
    if self.start_date > DateTime.now - 30.days
      return 100
    end
    if self.created_at > DateTime.now - 30.days
      return 100
    end      
    #Verifica quantas semanas possuem historicos nos ultimos 60 dias.
    qdd_posts = histories.where("histories.created_at > ?", DateTime.now - 60.days).group_by{ |u| u.created_at.beginning_of_month }.count

    #Determina o número de semanas
    qdd_semanas = (DateTime.now.to_date - self.start_date).to_i / 7
    if qdd_semanas > 8
      qdd_semanas = 8
    end
    (qdd_posts*100)/qdd_semanas
  end

  def calc_delay
    steps.where(status: :atrasada).sum(:percent)
  end

  def calc_indice_pontualidade
    100 - calc_delay
  end

  def calc_indice_execucao
    steps.where(status: :finalizada).sum(:percent)
  end

  def calc_status
    if steps.where(status: :atrasada).present?
      :atrasado
    elsif steps.where(status: :em_andamento).present?
      :em_andamento
    elsif calc_indice_execucao == 100
      :finalizado
    else
      :nao_iniciado
    end
  end

  def self.set_indices
    Project.all.map do |project|
      begin
        project.indice_pontualidade = project.calc_indice_pontualidade
        project.indice_atualizacao = project.calc_indice_atualizacao
        project.indice_execucao = project.calc_indice_execucao
        project.delay = project.calc_delay
        project.status = project.calc_status
        if project.save
           puts "sucesso id: #{project.id}"
        else
           puts "error id: #{project.id} [nao salvo]"
        end
      rescue
        project.indice_pontualidade = 0
        project.indice_atualizacao = 0
        project.indice_execucao = 0
        project.status = :nao_iniciado
        project.save
        puts "error id: #{project.id}"
      end
    end
  end

  def set_default_indices
    self.indice_pontualidade = self.indice_pontualidade|| 100
    self.indice_atualizacao = self.indice_atualizacao|| 100
    self.indice_execucao = self.indice_execucao|| 0
  end

  def self.create_by_decision(decision, texto)
    project = Project.new
    project.name = "Decisao nº #{decision.id} - #{texto}"
    project.budget = 0
    project.committed = 0
    project.paid = 0
    project.user = decision.user
    project.description = decision.description
    project.decision = decision
    step = project.steps.build 
    step.name = "Finalizar"
    step.start_date = DateTime.now.to_date
    step.estimated_date = decision.date
    step.description = decision.description
    project.save
    project.update_estimates
  end

  def orcamento_data
    ids = project_deeds.map { |e| e.deed }
    Prg.call(:get, "/api/search", {q: {deed_id_in: ids}})
  end

  def get_budget_info
    if project_deeds.present?      
      self.budget = orcamento_data[:autorizado].to_d.round(2)
      self.committed = orcamento_data[:empenhado].to_d.round(2)
      self.paid = orcamento_data[:liquidado].to_d.round(2)
      self.changes_amount = orcamento_data[:alteracoes].to_d.round(2)
    end
  end

  def executado_financeiro
    begin
      ((paid*100)/budget).to_i
    rescue
      0
    end
  end

  def user_can_edit(current_user)
    if user == current_user
      true
    elsif project_users.where(user: current_user, role: :editar).present?
      true
    else
      false
    end
  end

  def user_can_feed(current_user)
    if user == current_user
      true
    elsif project_users.where(user: current_user, role: :alimentar).present?
      true
    else
      false
    end
  end

end
