json.array!(@year_histories) do |year_history|
  json.extract! year_history, :id, :name, :description, :date, :indicator_id, :user_id
  json.url year_history_url(year_history, format: :json)
end
