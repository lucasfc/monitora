json.array!(@raes) do |rae|
  json.extract! rae, :id, :date, :ata
  json.url rae_url(rae, format: :json)
end
