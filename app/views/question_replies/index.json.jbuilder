json.array!(@question_replies) do |question_reply|
  json.extract! question_reply, :id, :question_id, :reply_id, :description
  json.url question_reply_url(question_reply, format: :json)
end
