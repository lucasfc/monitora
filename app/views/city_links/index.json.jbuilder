json.array!(@city_links) do |city_link|
  json.extract! city_link, :id, :category, :link
  json.url city_link_url(city_link, format: :json)
end
