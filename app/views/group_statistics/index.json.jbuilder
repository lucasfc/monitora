json.array!(@group_statistics) do |group_statistic|
  json.extract! group_statistic, :id, :name, :description
  json.url group_statistic_url(group_statistic, format: :json)
end
