json.array!(@decisions) do |decision|
  json.extract! decision, :id, :user_id, :rae_id, :description, :status, :date
  json.url decision_url(decision, format: :json)
end
