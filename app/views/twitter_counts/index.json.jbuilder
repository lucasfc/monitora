json.array!(@twitter_counts) do |twitter_count|
  json.extract! twitter_count, :id, :twitter_poll_id, :text, :value, :group_name, :good
  json.url twitter_count_url(twitter_count, format: :json)
end
