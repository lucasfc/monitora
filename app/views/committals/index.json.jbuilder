json.array!(@committals) do |committal|
  json.extract! committal, :id, :name, :description
  json.url committal_url(committal, format: :json)
end
