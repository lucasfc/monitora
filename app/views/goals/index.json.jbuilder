json.array!(@goals) do |goal|
  json.extract! goal, :id, :indicator_id, :date, :value
  json.url goal_url(goal, format: :json)
end
