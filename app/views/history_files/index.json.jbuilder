json.array!(@history_files) do |history_file|
  json.extract! history_file, :id, :name, :file, :history_id, :type
  json.url history_file_url(history_file, format: :json)
end
