json.array!(@statistic_histories) do |statistic_history|
  json.extract! statistic_history, :id, :statistic_id, :date, :value, :caret
  json.url statistic_history_url(statistic_history, format: :json)
end
