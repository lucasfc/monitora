json.array!(@statistics) do |statistic|
  json.extract! statistic, :id, :name, :description, :analize, :group_statistcs_id, :measure, :color, :icon
  json.url statistic_url(statistic, format: :json)
end
