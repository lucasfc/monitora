json.array!(@histories) do |history|
  json.extract! history, :id, :name, :step_id, :description, :user_id, :finish
  json.url history_url(history, format: :json)
end
