json.array!(@news) do |news|
  json.extract! news, :id, :title, :description, :link, :status
  json.url news_url(news, format: :json)
end
