json.array!(@twitter_polls) do |twitter_poll|
  json.extract! twitter_poll, :id, :name, :description
  json.url twitter_poll_url(twitter_poll, format: :json)
end
