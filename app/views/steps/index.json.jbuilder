json.array!(@steps) do |step|
  json.extract! step, :id, :name, :estimated_date, :finish_date, :status, :description, :percent, :project_id
  json.url step_url(step, format: :json)
end
