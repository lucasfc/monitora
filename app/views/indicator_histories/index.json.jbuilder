json.array!(@indicator_histories) do |indicator_history|
  json.extract! indicator_history, :id, :indicator_id, :date, :value, :status
  json.url indicator_history_url(indicator_history, format: :json)
end
