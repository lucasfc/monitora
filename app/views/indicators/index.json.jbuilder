json.array!(@indicators) do |indicator|
  json.extract! indicator, :id, :name, :type, :description, :referencial, :referencial_measure, :sumary, :suggestion
  json.url indicator_url(indicator, format: :json)
  json.gestor_name indicator.gestor_name
  json.last_data indicator.last_data
  json.next_goal indicator.next_goal
end
