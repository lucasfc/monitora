json.array!(@projects) do |project|
  json.extract! project, :id, :name, :budget, :committed, :paid, :user_id, :status, :description, :percent, :estimated_date, :finish_date
  json.url project_url(project, format: :json)
end
