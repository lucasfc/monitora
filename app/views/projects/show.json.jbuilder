json.extract! @project, :id, :name, :budget, :committed, :paid, :user_id, :status, :description, :percent, :estimated_date, :finish_date, :created_at, :updated_at
