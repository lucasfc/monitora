json.array!(@replies) do |reply|
  json.extract! reply, :id, :indicator_id, :user_id, :sumary, :suggestion
  json.url reply_url(reply, format: :json)
end
