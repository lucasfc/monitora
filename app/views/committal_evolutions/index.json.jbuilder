json.array!(@committal_evolutions) do |committal_evolution|
  json.extract! committal_evolution, :id, :committal_id, :orcamento, :empenhado, :liquidado, :executado_financeiro, :percentual_contratado, :executado_vs_contratado, :saldo, :executado_fisico
  json.url committal_evolution_url(committal_evolution, format: :json)
end
