class TwitterCountsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_twitter_count, only: [:show, :edit, :update, :destroy]

  # GET /twitter_counts
  # GET /twitter_counts.json
  def index
    @twitter_counts = TwitterCount.all
  end

  # GET /twitter_counts/1
  # GET /twitter_counts/1.json
  def show
  end

  # GET /twitter_counts/new
  def new
    @twitter_count = TwitterCount.new
  end

  # GET /twitter_counts/1/edit
  def edit
  end

  # POST /twitter_counts
  # POST /twitter_counts.json
  def create
    @twitter_count = TwitterCount.new(twitter_count_params)

    respond_to do |format|
      if @twitter_count.save
        format.html { redirect_to @twitter_count, notice: 'Twitter count was successfully created.' }
        format.json { render :show, status: :created, location: @twitter_count }
      else
        format.html { render :new }
        format.json { render json: @twitter_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /twitter_counts/1
  # PATCH/PUT /twitter_counts/1.json
  def update
    respond_to do |format|
      if @twitter_count.update(twitter_count_params)
        format.html { redirect_to @twitter_count, notice: 'Twitter count was successfully updated.' }
        format.json { render :show, status: :ok, location: @twitter_count }
      else
        format.html { render :edit }
        format.json { render json: @twitter_count.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twitter_counts/1
  # DELETE /twitter_counts/1.json
  def destroy
    @twitter_count.destroy
    respond_to do |format|
      format.html { redirect_to twitter_counts_url, notice: 'Twitter count was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twitter_count
      @twitter_count = TwitterCount.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twitter_count_params
      params.require(:twitter_count).permit(:twitter_poll_id, :text, :value, :group_name, :good)
    end
end
