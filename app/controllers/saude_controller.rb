class SaudeController < ApplicationController
  def index
  end

  def medicamentos
    redirect_to "http://sistemas.saude.to.gov.br/paineis/index.php?actAppBase=ZXN0b3F1ZT1udW1lcm9z"
  end

  def estoque
    redirect_to "http://sistemas.saude.to.gov.br/paineis/index.php?actAppBase=cGFkcm9uaXphY2FvPXBhZHJvbml6YWRvcw=="
  end

  def cirurgias_eletivas
    #redirect_to "http://sistemas.saude.to.gov.br/Eletivas/gerenciamento"
  end

  def ocupacao
    redirect_to "http://sistemas.saude.to.gov.br/paineis/index.php?actAppBase=b2N1cGFjYW89b2N1cGFjYW9faG9zcGl0YWlz"
  end
end
