class ProjectsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :orcamento]
  load_and_authorize_resource except: :create
  before_action :set_project, only: [:show, :edit, :update, :destroy, :orcamento]
  before_action :get_deeds, only: [:new, :edit, :update, :create]
  # GET /projects
  # GET /projects.json
  def index
    @q = Project.search(params[:q])
    @projects = @q.result.page(params[:page]).per(12)
  end

  def my
    @q = current_user.projects.search(params[:q])
    @projects = @q.result.page(params[:page]).per(12)
    render :index
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @history = History.new
    if params[:step].present?
      @histories = @project.histories.where(step_id: params[:step].to_i).page(params[:page])
    else      
      @histories = Kaminari.paginate_array(@project.all_histories.sort_by{ |e| e.created_at}.reverse).page(params[:page])
    end
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
        @project.update_estimates
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
        @project.update_estimates
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def orcamento
    @data = @project.orcamento_data
    @title = @project.name
    render "powerbi/mobile_geral"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    def get_deeds
      @users = User.all.map { |u| [u.name,u.id]  }
      @deeds = Prg.call(:get, "/deeds.json", {fonte: params[:fonte]}).map { |d| ["#{d[:siafem_code]} - #{d[:name]}", d[:id]] }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(
        :name, :budget, :committed, :paid, :user_id, :status,
        :description, :percent, :estimated_date, :finish_date,
        :indicator_id, :project_id, :lat, :lng, :committal_id,
        :changes_amount, :live,
        steps_attributes:[
          :id, :name, :estimated_date, :finish_date, :status, 
          :description, :percent, :project_id, :start_date, :_destroy
        ],
        project_deeds_attributes:[
          :id, :deed, :project_id, :_destroy
        ],
        project_users_attributes:[
          :id, :user_id, :project_id, :role, :_destroy
        ]
      )
    end
end
