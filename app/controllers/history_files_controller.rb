class HistoryFilesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_history_file, only: [:show, :edit, :update, :destroy]

  # GET /history_files
  # GET /history_files.json
  def index
    @history_files = HistoryFile.all
  end

  # GET /history_files/1
  # GET /history_files/1.json
  def show
  end

  # GET /history_files/new
  def new
    @history_file = HistoryFile.new
  end

  # GET /history_files/1/edit
  def edit
  end

  # POST /history_files
  # POST /history_files.json
  def create
    @history_file = HistoryFile.new(history_file_params)

    respond_to do |format|
      if @history_file.save
        format.html { redirect_to @history_file, notice: 'History file was successfully created.' }
        format.json { render :show, status: :created, location: @history_file }
      else
        format.html { render :new }
        format.json { render json: @history_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /history_files/1
  # PATCH/PUT /history_files/1.json
  def update
    respond_to do |format|
      if @history_file.update(history_file_params)
        format.html { redirect_to @history_file, notice: 'History file was successfully updated.' }
        format.json { render :show, status: :ok, location: @history_file }
      else
        format.html { render :edit }
        format.json { render json: @history_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /history_files/1
  # DELETE /history_files/1.json
  def destroy
    @history_file.destroy
    respond_to do |format|
      format.html { redirect_to history_files_url, notice: 'History file was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_history_file
      @history_file = HistoryFile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def history_file_params
      params.require(:history_file).permit(:name, :file, :history_id, :type)
    end
end
