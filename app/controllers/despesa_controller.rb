class DespesaController < ApplicationController
	before_action :get_combos, only:[:comparativo, :natureza, :dea, :insuficiencia]
	def avaliacao
	end

	def relatorio
	end

	def pdris
		@contratos =  PdrisPainel.contratados.group_by(&:nome_grupo)
		render "publico/pdris"
	end

	def pessoal
		@q = Secad.search(params[:q])
		@servidores = @q.result
		@counter = @servidores.select('distinct cpf').count
		@orgaos = Secad.select(:orgao).uniq.map{|a| a.orgao}
		@cargos = Secad.select(:cargo).uniq.map{|a| a.cargo}
		@funcoes = Secad.select(:referencia_funcao_).uniq.map{|a| a.referencia_funcao_}
		@vinculos = Secad.select(:tipo_vinculo).uniq.map{|a| a.tipo_vinculo}
	end

	def comparativo
			@data = Prg.call(:get, "/api/allocations/comparative", {date: params[:date], q: params[:q]})
	end

	def natureza
		@data = Prg.call(:get, "/api/allocations/natures", {date: params[:date], q: params[:q]})
	end

	def dea
		@data = Prg.call(:get, "/api/allocations/dea", {date: params[:date], q: params[:q]})
	end

	def insuficiencia
		@data = Prg.call(:get, "/api/allocations/sources", {date: params[:date], q: params[:q]})
	end

	def balanco
		@data = Prg.call(:get, "/api/balanco", {})
	end

	private
		def get_combos
			@sources = Prg.call(:get, "/sources.json", {}).map { |e| [e[:name], e[:id]]  }
			@natures = Prg.call(:get, "/natures.json", {}).map { |e| [e[:name], e[:id]]  }
			@modalities = Prg.call(:get, "/modalities.json", {}).map { |e| [e[:name], e[:id]]  }
			@entities = Prg.call(:get, "/entities.json", {}).map { |e| [e[:name], e[:id]]  }
			@deeds = Prg.call(:get, "/deeds.json", {}).map { |e| ["#{e[:siafem_code]} - #{e[:name]} #{'* PRIORITÁRIA' if e[:priority]}", e[:id]]  }
		end
end
