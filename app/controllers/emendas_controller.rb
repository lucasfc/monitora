class EmendasController < ApplicationController
	before_action :authenticate_user!
	authorize_resource class: :emendas
	def index					
		@deputados = Emendas.get_resume(params[:year])
	end

	def resume
		emendas = Emendas.get_json(params[:year])
		if params[:source].present?
			row = params[:year].present? ? 8 : 1
			emendas = emendas.select{|a| a[row] == params[:source] }
		end
		@emendas = emendas
	end
end
