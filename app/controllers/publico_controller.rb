class PublicoController < ApplicationController
	layout 'publico'
	skip_before_action :verify_authenticity_token, only: :widget
	def index
	end

	def orcamento
		@data = Prg.call(:get, "/api/index", {}) 
		@sources = Prg.call(:get, "/sources.json", {}).map { |e| [e[:name], e[:id]]  }
		@natures = Prg.call(:get, "/natures.json", {}).map { |e| [e[:name], e[:id]]  }
		@modalities = Prg.call(:get, "/modalities.json", {}).map { |e| [e[:name], e[:id]]  }
		@entities = Prg.call(:get, "/entities.json", {}).map { |e| [e[:name], e[:id]]  }
		@deeds = Prg.call(:get, "/deeds.json", {}).map { |e| ["#{e[:siafem_code]} - #{e[:name]} #{'* PRIORITÁRIA' if e[:priority]}", e[:id]]  }
		@public = true
		render "powerbi/search"
	end

	def entregas
		@committals = Committal.all
	end

	def pdris
		@contratos =  PdrisPainel.contratados.group_by(&:nome_grupo)
	end

	def indicadores
		@q = Indicator.externals.search(params[:q])
    	@indicators = @q.result.page(params[:page])
    	render "indicators/index"
	end

	def despesa		
	end

	def widget
		@indicator = Indicator.find(4)
		respond_to :js
	end

	def secretario_pre
		
	end

	def secretario
		if params[:code] == "291019902607201106011996"
			@user = User.find_by_email("david.torres@seplan.to.gov.br")
			sign_in_and_redirect @user, :event => :authentication
		end
	end

	protected

	def authenticate
	  authenticate_or_request_with_http_basic do |username, password|
	    username == "seplan" && password == "monitorando"
	  end
	end
end
