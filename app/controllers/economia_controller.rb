class EconomiaController < ApplicationController
  def index
  end

  def subitem
    @entities = Prg.call(:get, "/entities.json", {}).map { |e| ["#{e[:name]} - #{e[:siafem_code]} ", e[:siafem_code]]  }
    @data = Prg.call(:get, "/api/relpdug/subitem_month", {q: params[:q], subitem: params[:subitem], ug: params[:ug]})
    meta = (@data[:current_year].take(5).sum{|a| a[:value].to_i} / 5)
    @data_chart = [
        {name: "Ano Anteriror", data: @data[:last_year].map{|a| [I18n.t("date.month_names")[a[:month]], a[:value]] }},
        {name: "Meta", data: @data[:last_year].map{|a| [I18n.t("date.month_names")[a[:month]], meta] }},
        {name: "Corrente Ano", data: @data[:current_year].map{|a| [I18n.t("date.month_names")[a[:month]], a[:value]] }}
    ]
  end

  def sgd
  end

  def ranking
    @month = params[:month] || Date.today.month - 1
    @month = @month.to_i
    @month_name = Date::MONTHNAMES[@month]
    @data = Prg.call(:get, "/api/ranking", {q: params[:q], month: @month, sources:[1,19,24]}).sort_by{|a| a[:reduction].to_f}
  end
end
