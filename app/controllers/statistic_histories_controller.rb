class StatisticHistoriesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_statistic_history, only: [:show, :edit, :update, :destroy]

  # GET /statistic_histories
  # GET /statistic_histories.json
  def index
    @statistic_histories = StatisticHistory.all
  end

  # GET /statistic_histories/1
  # GET /statistic_histories/1.json
  def show
  end

  # GET /statistic_histories/new
  def new
    @statistic_history = StatisticHistory.new
  end

  # GET /statistic_histories/1/edit
  def edit
  end

  # POST /statistic_histories
  # POST /statistic_histories.json
  def create
    @statistic_history = StatisticHistory.new(statistic_history_params)

    respond_to do |format|
      if @statistic_history.save
        format.html { redirect_to @statistic_history, notice: 'Statistic history was successfully created.' }
        format.json { render :show, status: :created, location: @statistic_history }
      else
        format.html { render :new }
        format.json { render json: @statistic_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statistic_histories/1
  # PATCH/PUT /statistic_histories/1.json
  def update
    respond_to do |format|
      if @statistic_history.update(statistic_history_params)
        format.html { redirect_to @statistic_history, notice: 'Statistic history was successfully updated.' }
        format.json { render :show, status: :ok, location: @statistic_history }
      else
        format.html { render :edit }
        format.json { render json: @statistic_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statistic_histories/1
  # DELETE /statistic_histories/1.json
  def destroy
    @statistic_history.destroy
    respond_to do |format|
      format.html { redirect_to statistic_histories_url, notice: 'Statistic history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_statistic_history
      @statistic_history = StatisticHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def statistic_history_params
      params.require(:statistic_history).permit(:statistic_id, :date, :value, :caret)
    end
end
