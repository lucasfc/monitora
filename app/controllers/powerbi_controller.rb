class PowerbiController < ApplicationController
  before_action :authenticate_user!, except: [:search_result, :receita, :receita_result, :search]
  def orcamento
  end

  def receita
    @data = Prg.call(:get, "/api/incomes", {}) 
    @names = Prg.call(:get, "/incomes.json", {}).map { |e| ["#{e[:name]} (#{e[:origin]})", e[:id]]  }
    @categories = Prg.call(:get, "/income_categories.json", {}).map { |e| [e[:name], e[:id]]  }
    @origins = Prg.call(:get, "/income_origins.json", {}).map { |e| [e[:name], e[:id]]  }
    respond_to :js, :html
  end

  def receita_evolution
    @months= [
      nil, "Janeiro", "Fevereiro", "Março", "Abril", "Maio",
      "Junho", "Julho", "Agosto", "Setembro","Outubro", "Novembro",
      "Dezembro"
    ]
    @data = Prg.call(:get, "/api/evolution", {}) 
  end

  def evolucao
  end

  def mobile_geral
    @data = Prg.call(:get, "/api/index", {}) 
    @title = "Todas as Fontes"
  end
  
  def fonte
    @data = Prg.call(:get, "/api/sources", {fonte: params[:fonte]}) 
    @title = "Orçamento Fonte #{params[:fonte]}"  
    render :mobile_geral
  end

  def search
    @data = Prg.call(:get, "/api/index", {q: params[:q]})
    @sources = Prg.call(:get, "/sources.json", {}).map { |e| [e[:name], e[:id]]  }
    @natures = Prg.call(:get, "/natures.json", {}).map { |e| [e[:name], e[:id]]  }
    @modalities = Prg.call(:get, "/modalities.json", {}).map { |e| [e[:name], e[:id]]  }
    @entities = Prg.call(:get, "/entities.json", {}).map { |e| [e[:name], e[:id]]  }
    @deeds = Prg.call(:get, "/deeds.json", {}).map { |e| ["#{e[:siafem_code]} - #{e[:name]} #{'* PRIORITÁRIA' if e[:priority]}", e[:id]]  }
    respond_to :js, :html
  end
  
  def search_result
    @data = Prg.call(:get, "/api/search", {q: params[:q]}) 
    @title = "Resultado da Pesquisa"
    #binding.pry
    respond_to do |format|
      #format.html {render :mobile_geral}
      format.js
    end
  end

  def receita_result
    @data = Prg.call(:get, "/api/incomes", {q: params[:q]}) 
    @title = "Resultado da Pesquisa"
    respond_to do |format|
      #format.html {render :mobile_geral}
      format.js
    end
  end

end
