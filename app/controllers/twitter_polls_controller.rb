class TwitterPollsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_twitter_poll, only: [:show, :edit, :update, :destroy]

  # GET /twitter_polls
  # GET /twitter_polls.json
  def index
    @twitter_polls = TwitterPoll.all
  end

  # GET /twitter_polls/1
  # GET /twitter_polls/1.json
  def show
  end

  # GET /twitter_polls/new
  def new
    @twitter_poll = TwitterPoll.new
  end

  # GET /twitter_polls/1/edit
  def edit
  end

  # POST /twitter_polls
  # POST /twitter_polls.json
  def create
    @twitter_poll = TwitterPoll.new(twitter_poll_params)

    respond_to do |format|
      if @twitter_poll.save
        format.html { redirect_to @twitter_poll, notice: 'Twitter poll was successfully created.' }
        format.json { render :show, status: :created, location: @twitter_poll }
      else
        format.html { render :new }
        format.json { render json: @twitter_poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /twitter_polls/1
  # PATCH/PUT /twitter_polls/1.json
  def update
    respond_to do |format|
      if @twitter_poll.update(twitter_poll_params)
        format.html { redirect_to @twitter_poll, notice: 'Twitter poll was successfully updated.' }
        format.json { render :show, status: :ok, location: @twitter_poll }
      else
        format.html { render :edit }
        format.json { render json: @twitter_poll.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /twitter_polls/1
  # DELETE /twitter_polls/1.json
  def destroy
    @twitter_poll.destroy
    respond_to do |format|
      format.html { redirect_to twitter_polls_url, notice: 'Twitter poll was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_twitter_poll
      @twitter_poll = TwitterPoll.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def twitter_poll_params
      params.require(:twitter_poll).permit(
        :name, :description,
      twitter_counts_attributes:[
        :id, :twitter_poll_id, :text, :value, :group_name, :good, :_destroy
      ]
      )
    end
end
