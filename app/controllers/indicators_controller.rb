class IndicatorsController < ApplicationController
  before_action :authenticate_user!, except: :show
  load_and_authorize_resource except: :create
  before_action :set_indicator, only: [:show, :edit, :update,
    :destroy, :goals, :histories, :entregas_combo, :analyze, :year_history]

  # GET /indicators
  # GET /indicators.json
  def index     
    @q = Indicator.search(params[:q])
    @indicators = @q.result.page(params[:page])
  end

  def report
    @indicators = Indicator.all
    respond_to do |format|
      format.html do
        render layout: "pdf.html"
      end
      format.pdf do
        render pdf: "file_name",
        layout: "pdf.html",
        disable_javascript: false,
        window_status: :ready,
        show_as_html: params.key?('debug'),
        book: true,
        cover: "<img width='100%' src='file:////#{Rails.root}/app/assets/images/relatorios/capa-indicadores.png' >",
        toc: {header_text:"Sumário",}
      end
    end
  end

  # GET /indicators/1
  # GET /indicators/1.json
  def show
    @indicator_user = @indicator.user.toauth_fast
    @indicator_gestor = @indicator.gestor.toauth_fast
    @goals = @indicator.next_goals.take(4)
    @histories = @indicator.indicator_histories.order(date: :desc).limit(5)
    if @indicator.replies.present?
      @reply = @indicator.replies.order(finish_date: :desc).first
    end
    if user_signed_in?
      @deeds = @indicator.planeja_deeds(current_user)
      render :show_gestor
    end
  end

  def analyze    
  end

  # GET /indicators/new
  def new
    @indicator = Indicator.new
  end

  # GET /indicators/1/edit
  def edit
  end

  # POST /indicators
  # POST /indicators.json
  def create
    @indicator = Indicator.new(indicator_params)

    respond_to do |format|
      if @indicator.save
        format.html { redirect_to @indicator, notice: 'Indicator was successfully created.' }
        format.json { render :show, status: :created, location: @indicator }
      else
        format.html { render :new }
        format.json { render json: @indicator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /indicators/1
  # PATCH/PUT /indicators/1.json
  def update
    respond_to do |format|
      if @indicator.update(indicator_params)
        format.html { redirect_to @indicator, notice: 'Indicator was successfully updated.' }
        format.json { render :show, status: :ok, location: @indicator }
      else
        format.html { render :edit }
        format.json { render json: @indicator.errors, status: :unprocessable_entity }
      end
    end
  end

  def deed_evaluations
    
  end

  # DELETE /indicators/1
  # DELETE /indicators/1.json
  def destroy
    @indicator.destroy
    respond_to do |format|
      format.html { redirect_to indicators_url, notice: 'Indicator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def goals
    @goals = @indicator.goals.order(date: :asc ).page(params[:page])
  end

  def histories
    @histories = @indicator.indicator_histories.order(date: :asc ).page(params[:page])
  end

  def entregas_combo
    respond_to :js
  end

  def probabilidade
    respond_to :js
  end

  def year_history
    @year_histories = @indicator.year_histories.order(date: :asc).group_by(&:date)
    render "year_histories/index"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_indicator
      @indicator = Indicator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def indicator_params
      params.require(:indicator).permit(
        :name, :type, :description, :user_id, :gestor_id, :identification, :contract,
        :icon, :color, :measure, :ord, :referencial, :referencial_measure,
        :twitter_poll_id, 
        goals_attributes:[
          :id, :indicator_id, :date, :value, :_destroy
        ],
        committals_attributes:[
          :id, :name, :description, :indicator_id, :_destroy
        ]
        )
    end
end
