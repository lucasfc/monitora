class PdrisController < ApplicationController
	before_action :authenticate_user!
	def index
		@q = PdrisObj.search(params[:q])
		@projects = @q.result.page(params[:page]).per(12)
	end

	def show
		@project = PdrisObj.find(params[:id])
	end
end
