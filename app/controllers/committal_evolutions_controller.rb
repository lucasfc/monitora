class CommittalEvolutionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_committal_evolution, only: [:show, :edit, :update, :destroy]

  # GET /committal_evolutions
  # GET /committal_evolutions.json
  def index
    @committal_evolutions = CommittalEvolution.all
  end

  # GET /committal_evolutions/1
  # GET /committal_evolutions/1.json
  def show
  end

  # GET /committal_evolutions/new
  def new
    @committal_evolution = CommittalEvolution.new
  end

  # GET /committal_evolutions/1/edit
  def edit
  end

  # POST /committal_evolutions
  # POST /committal_evolutions.json
  def create
    @committal_evolution = CommittalEvolution.new(committal_evolution_params)

    respond_to do |format|
      if @committal_evolution.save
        format.html { redirect_to @committal_evolution, notice: 'Committal evolution was successfully created.' }
        format.json { render :show, status: :created, location: @committal_evolution }
      else
        format.html { render :new }
        format.json { render json: @committal_evolution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /committal_evolutions/1
  # PATCH/PUT /committal_evolutions/1.json
  def update
    respond_to do |format|
      if @committal_evolution.update(committal_evolution_params)
        format.html { redirect_to @committal_evolution, notice: 'Committal evolution was successfully updated.' }
        format.json { render :show, status: :ok, location: @committal_evolution }
      else
        format.html { render :edit }
        format.json { render json: @committal_evolution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /committal_evolutions/1
  # DELETE /committal_evolutions/1.json
  def destroy
    @committal_evolution.destroy
    respond_to do |format|
      format.html { redirect_to committal_evolutions_url, notice: 'Committal evolution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_committal_evolution
      @committal_evolution = CommittalEvolution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def committal_evolution_params
      params.require(:committal_evolution).permit(:committal_id, :orcamento, :empenhado, :liquidado, :executado_financeiro, :percentual_contratado, :executado_vs_contratado, :saldo, :executado_fisico)
    end
end
