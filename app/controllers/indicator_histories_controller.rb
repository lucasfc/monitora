class IndicatorHistoriesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_indicator_history, only: [:show, :edit, :update, :destroy]
  before_action :get_indicators, only: [:new, :edit, :update, :create]

  # GET /indicator_histories
  # GET /indicator_histories.json
  def index
    @indicator_histories = IndicatorHistory.all
  end

  # GET /indicator_histories/1
  # GET /indicator_histories/1.json
  def show
  end

  # GET /indicator_histories/new
  def new
    @indicator_history = IndicatorHistory.new
  end

  # GET /indicator_histories/1/edit
  def edit
  end

  # POST /indicator_histories
  # POST /indicator_histories.json
  def create
    @indicator_history = IndicatorHistory.new(indicator_history_params)

    respond_to do |format|
      if @indicator_history.save
        format.html { redirect_to @indicator_history.indicator, notice: 'Indicator history was successfully created.' }
        format.json { render :show, status: :created, location: @indicator_history }
      else
        format.html { render :new }
        format.json { render json: @indicator_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /indicator_histories/1
  # PATCH/PUT /indicator_histories/1.json
  def update
    respond_to do |format|
      if @indicator_history.update(indicator_history_params)
        format.html { redirect_to @indicator_history, notice: 'Indicator history was successfully updated.' }
        format.json { render :show, status: :ok, location: @indicator_history }
      else
        format.html { render :edit }
        format.json { render json: @indicator_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /indicator_histories/1
  # DELETE /indicator_histories/1.json
  def destroy
    @indicator_history.destroy
    respond_to do |format|
      format.html { redirect_to indicator_histories_url, notice: 'Indicator history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_indicator_history
      @indicator_history = IndicatorHistory.find(params[:id])
    end

    def get_indicators
      if current_user.seplan? or current_user.admin?
        @indicators = Indicator.all
      else
        @indicators = current_user.indicators
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def indicator_history_params
      params.require(:indicator_history).permit(:indicator_id, :date, :value, :status)
    end
end
