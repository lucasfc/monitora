class RaesController < ApplicationController
    before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_rae, only: [:show, :edit, :update, :destroy]

  # GET /raes
  # GET /raes.json
  def index
    @q = Rae.search(params[:q])
    @raes = @q.result.page(params[:page])
  end

  # GET /raes/1
  # GET /raes/1.json
  def show
    @rae_user = @rae.user.toauth_fast
  end

  # GET /raes/new
  def new
    @rae = Rae.new
  end

  # GET /raes/1/edit
  def edit
  end

  # POST /raes
  # POST /raes.json
  def create
    @rae = Rae.new(rae_params)
    @rae.user = current_user
    respond_to do |format|
      if @rae.save
        format.html { redirect_to @rae, notice: 'Rae was successfully created.' }
        format.json { render :show, status: :created, location: @rae }
      else
        format.html { render :new }
        format.json { render json: @rae.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /raes/1
  # PATCH/PUT /raes/1.json
  def update
    respond_to do |format|
      if @rae.update(rae_params)
        format.html { redirect_to @rae, notice: 'Rae was successfully updated.' }
        format.json { render :show, status: :ok, location: @rae }
      else
        format.html { render :edit }
        format.json { render json: @rae.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /raes/1
  # DELETE /raes/1.json
  def destroy
    @rae.destroy
    respond_to do |format|
      format.html { redirect_to raes_url, notice: 'Rae was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rae
      @rae = Rae.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rae_params
      params.require(:rae).permit(
        :date, :ata, :user,
        decisions_attributes:[
          :id, :user_id, :rae_id, :description, :status, :date
        ]
      )
    end
end
