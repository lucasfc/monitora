class ConveniosController < ApplicationController
	#before_action :authenticate_user!
	#authorize_resource class: :convenios
	def index
		@orgaos = Convenios.get_resume.sort { |a, b| a['name'] <=> b  ['name']}
	end

	def filter
		convenios = Convenios.get_json
		if params[:orgao].present?
			convenios = convenios.select{|a| a[0] == params[:orgao] }
		end
		@convenios = convenios
	end
end
