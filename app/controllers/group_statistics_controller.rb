class GroupStatisticsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_group_statistic, only: [:show, :edit, :update, :destroy]
  before_action :icons_list, only: [:new, :edit, :update, :create]

  # GET /group_statistics
  # GET /group_statistics.json
  def index
    @group_statistics = GroupStatistic.all
  end

  # GET /group_statistics/1
  # GET /group_statistics/1.json
  def show
  end

  # GET /group_statistics/new
  def new
    @group_statistic = GroupStatistic.new
  end

  # GET /group_statistics/1/edit
  def edit
  end

  # POST /group_statistics
  # POST /group_statistics.json
  def create
    @group_statistic = GroupStatistic.new(group_statistic_params)

    respond_to do |format|
      if @group_statistic.save
        format.html { redirect_to @group_statistic, notice: 'Group statistic was successfully created.' }
        format.json { render :show, status: :created, location: @group_statistic }
      else
        format.html { render :new }
        format.json { render json: @group_statistic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /group_statistics/1
  # PATCH/PUT /group_statistics/1.json
  def update
    respond_to do |format|
      if @group_statistic.update(group_statistic_params)
        format.html { redirect_to @group_statistic, notice: 'Group statistic was successfully updated.' }
        format.json { render :show, status: :ok, location: @group_statistic }
      else
        format.html { render :edit }
        format.json { render json: @group_statistic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /group_statistics/1
  # DELETE /group_statistics/1.json
  def destroy
    @group_statistic.destroy
    respond_to do |format|
      format.html { redirect_to group_statistics_url, notice: 'Group statistic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_group_statistic
      @group_statistic = GroupStatistic.find(params[:id])
    end

    def icons_list
      file = open("#{Rails.root}/public/fa-list.json")
      @icons = JSON.load(file)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def group_statistic_params
      params.require(:group_statistic).permit(:name, :description, :icon, :color)
    end
end
