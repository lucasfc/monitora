class QuestionRepliesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_question_reply, only: [:show, :edit, :update, :destroy]

  # GET /question_replies
  # GET /question_replies.json
  def index
    @question_replies = QuestionReply.all
  end

  # GET /question_replies/1
  # GET /question_replies/1.json
  def show
  end

  # GET /question_replies/new
  def new
    @question_reply = QuestionReply.new
  end

  # GET /question_replies/1/edit
  def edit
  end

  # POST /question_replies
  # POST /question_replies.json
  def create
    @question_reply = QuestionReply.new(question_reply_params)

    respond_to do |format|
      if @question_reply.save
        format.html { redirect_to @question_reply, notice: 'Question reply was successfully created.' }
        format.json { render :show, status: :created, location: @question_reply }
      else
        format.html { render :new }
        format.json { render json: @question_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /question_replies/1
  # PATCH/PUT /question_replies/1.json
  def update
    respond_to do |format|
      if @question_reply.update(question_reply_params)
        format.html { redirect_to @question_reply, notice: 'Question reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @question_reply }
      else
        format.html { render :edit }
        format.json { render json: @question_reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /question_replies/1
  # DELETE /question_replies/1.json
  def destroy
    @question_reply.destroy
    respond_to do |format|
      format.html { redirect_to question_replies_url, notice: 'Question reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question_reply
      @question_reply = QuestionReply.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_reply_params
      params.require(:question_reply).permit(:question_id, :reply_id, :description)
    end
end
