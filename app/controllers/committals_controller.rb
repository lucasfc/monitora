class CommittalsController < ApplicationController
  before_action :authenticate_user!, except: [:projects, :show]
  load_and_authorize_resource except: :create
  before_action :set_committal, only: [:show, :edit, :update, :destroy, :projects]

  # GET /committals
  # GET /committals.json
  def index
    @committals = Committal.all
  end

  # GET /committals/1
  # GET /committals/1.json
  def show
  end

  def projects    
  end

  # GET /committals/new
  def new
    @committal = Committal.new
  end

  # GET /committals/1/edit
  def edit
  end

  # POST /committals
  # POST /committals.json
  def create
    @committal = Committal.new(committal_params)

    respond_to do |format|
      if @committal.save
        format.html { redirect_to @committal, notice: 'Committal was successfully created.' }
        format.json { render :show, status: :created, location: @committal }
      else
        format.html { render :new }
        format.json { render json: @committal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /committals/1
  # PATCH/PUT /committals/1.json
  def update
    respond_to do |format|
      if @committal.update(committal_params)
        format.html { redirect_to @committal, notice: 'Committal was successfully updated.' }
        format.json { render :show, status: :ok, location: @committal }
      else
        format.html { render :edit }
        format.json { render json: @committal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /committals/1
  # DELETE /committals/1.json
  def destroy
    @committal.destroy
    respond_to do |format|
      format.html { redirect_to committals_url, notice: 'Committal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_committal
      @committal = Committal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def committal_params
      params.require(:committal).permit(:name, :description)
    end
end
