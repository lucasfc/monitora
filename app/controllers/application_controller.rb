class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_toauth_data
  before_filter :set_paper_trail_whodunnit
  layout :layout_by_resource

  def layout_by_resource
    if !user_signed_in?      
      "publico"
    else
      "application"
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    cookies.signed[:toauth_fast] = nil
    ToauthConnect.logout
  end

  def set_toauth_data
  	#binding.pry
  	if user_signed_in?
  		if cookies.signed[:toauth_fast].blank?  			
  			cookies.signed[:toauth_fast] = { value: current_user.toauth_fast.to_json, expires: 3.hour.from_now }
  		end
  		@toauth_fast = JSON.parse(cookies.signed[:toauth_fast], symbolize_names: true)
  	end
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to '/403.html'
  end
end
