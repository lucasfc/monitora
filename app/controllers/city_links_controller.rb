class CityLinksController < ApplicationController
  load_and_authorize_resource except: :create
  before_action :set_city_link, only: [:show, :edit, :update, :destroy]

  # GET /city_links
  # GET /city_links.json
  def index
    @city_links = CityLink.all
  end

  # GET /city_links/1
  # GET /city_links/1.json
  def show
  end

  # GET /city_links/new
  def new
    @city_link = CityLink.new
  end

  # GET /city_links/1/edit
  def edit
  end

  # POST /city_links
  # POST /city_links.json
  def create
    @city_link = CityLink.new(city_link_params)

    respond_to do |format|
      if @city_link.save
        format.html { redirect_to @city_link, notice: 'City link was successfully created.' }
        format.json { render :show, status: :created, location: @city_link }
      else
        format.html { render :new }
        format.json { render json: @city_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /city_links/1
  # PATCH/PUT /city_links/1.json
  def update
    respond_to do |format|
      if @city_link.update(city_link_params)
        format.html { redirect_to @city_link, notice: 'City link was successfully updated.' }
        format.json { render :show, status: :ok, location: @city_link }
      else
        format.html { render :edit }
        format.json { render json: @city_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /city_links/1
  # DELETE /city_links/1.json
  def destroy
    @city_link.destroy
    respond_to do |format|
      format.html { redirect_to city_links_url, notice: 'City link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city_link
      @city_link = CityLink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_link_params
      params.require(:city_link).permit(:category, :link)
    end
end
