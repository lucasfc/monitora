class HomeController < ApplicationController
	before_action :authenticate_user!, except: [:orcamento, :lrf, :tocantins_em_numeros, :cidades]
	authorize_resource class: :home, except: [:orcamento, :lrf, :cidades]
	def index
	end

	def gestores
	end

	def imprensa
		endereco = "https://www.googleapis.com/customsearch/v1?key=AIzaSyCj8ttov8TadRwafLqs7ic38P3IQnKNdOw&cx=002871630582686314731:wvbp-djnbww&q=tocantins+noticia&dateRestrict=d7"
		data = JSON.parse(RestClient.send(:get, endereco), symbolize_names: true)
		begin
			@data = data[:items]			
		rescue 
			@data= []			
		end
	end

	def redes_sociais
		@bem = TwitterCount.where(good: true).sum(:value)
		@mal = TwitterCount.where(good: [false, nil]).sum(:value)
		@polls = TwitterPoll.all
	end

	def orcamento
		if user_signed_in?
			@orcamento_link = despesa_menu_path
			@receita_link = receita_bi_path
		else
		 	@orcamento_link = pub_despesa_path
			@receita_link = receita_bi_path
		end
	end

	def cidades
		@cidades = JSON.parse(RestClient.get("http://cidades.monitora.to.gov.br/cidades.json")).map{|a| [a["text"], a["id"]]}
	end
end
