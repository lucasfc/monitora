class YearHistoriesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource except: :create
  before_action :set_year_history, only: [:show, :edit, :update, :destroy]

  # GET /year_histories
  # GET /year_histories.json
  def index
    @year_histories = YearHistory.all
  end

  # GET /year_histories/1
  # GET /year_histories/1.json
  def show
  end

  # GET /year_histories/new
  def new
    @year_history = YearHistory.new
  end

  # GET /year_histories/1/edit
  def edit
  end

  # POST /year_histories
  # POST /year_histories.json
  def create
    @year_history = YearHistory.new(year_history_params)
    @year_history.user = current_user
    respond_to do |format|
      if @year_history.save
        format.html { redirect_to year_history_indicator_path(@year_history.indicator), notice: 'Year history was successfully created.' }
        format.json { render :show, status: :created, location: @year_history }
      else
        format.html { render :new }
        format.json { render json: @year_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /year_histories/1
  # PATCH/PUT /year_histories/1.json
  def update
    respond_to do |format|
      if @year_history.update(year_history_params)
        format.html { redirect_to @year_history, notice: 'Year history was successfully updated.' }
        format.json { render :show, status: :ok, location: @year_history }
      else
        format.html { render :edit }
        format.json { render json: @year_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /year_histories/1
  # DELETE /year_histories/1.json
  def destroy
    @year_history.destroy
    respond_to do |format|
      format.html { redirect_to year_histories_url, notice: 'Year history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_year_history
      @year_history = YearHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def year_history_params
      params.require(:year_history).permit(:name, :description, :date, :indicator_id, :user_id)
    end
end
