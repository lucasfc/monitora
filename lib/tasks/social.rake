namespace :social do
	desc "Atualiza Projetos"
  	task refresh_counters: :environment do
  		puts "-Iniciando Atualização"
  		TwitterCount.refresh_tweets
  		puts "--Counters Atualizados!"
  	end
end
