namespace :update_projects do
	desc "Atualiza Projetos"
  	task update_steps: :environment do
  	puts "-Iniciando Atualização de etapas"
  		puts "--Atualizando Etapas"
  		Step.set_atrasadas
  		puts "--Etapas Atualizadas!"
  	end
  	task update_indices: :environment do
  	puts "-Iniciando Atualização de índices"
  		puts "--Atualizando Indices"
  		Project.set_indices
      Committal.run_calculations
  		puts "--Indices Atualizados!"
  	end
end
